using namespace std;
#include <cmath> 

void calc_weno(double d_t, double d_x, double d_p, double k_diff, int m, double * x, int n, double * p, double * prob_dens_old, double * Force_full, double max_p, double max_force, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * prob_dens_new, int i_min, int i_max, int j_min, int j_max);


void R_K_2_order_2_steps(int m, double * x, double d_x, int n, double * p, double d_p, double * W_pr, double k_diff, double d_t, double * Force_full, double max_p, double max_force, int i_mi, int i_ma, int j_mi, int j_ma, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * W_pr_new)
{
//____________TVD ����� ����� ������� �������, �����������, �����

int N_tot, i, j, k_k;
double a_p;

N_tot=m*n;
double *Prob_1 = new double [N_tot+1];			//������������� ������


calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, W_pr, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, Prob_1, i_mi, i_ma, j_mi, j_ma);	//u_1
calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, Prob_1, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, W_pr_new, i_mi, i_ma, j_mi, j_ma);	//u_1_2

a_p=0.5;
	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)
		{
		k_k=(i-1)*n+j;	
		W_pr_new[k_k]=a_p*( W_pr[k_k] + W_pr_new[k_k] );
		}				
	}


delete [] Prob_1;	

return;	
}

//_____________________________________________________________________________________________________________________________________________________________
void R_K_3_order_3_steps(int m, double * x, double d_x, int n, double * p, double d_p, double * W_pr, double k_diff, double d_t, double * Force_full, double max_p, double max_force, int i_mi, int i_ma, int j_mi, int j_ma, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * W_pr_new)
{
//____________TVD ����� ����� �������� �������, �����������, �����	

int N_tot, i, j, k_k;
double a_p, b_p;

N_tot=m*n;
double *Prob_1 = new double [N_tot+1];			//������������� ������

calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, W_pr, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, Prob_1, i_mi, i_ma, j_mi, j_ma);			//u_1
calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, Prob_1, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, W_pr_new, i_mi, i_ma, j_mi, j_ma);			//u_1_2


a_p=0.75;
b_p=0.25;
	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)
		{
		k_k=(i-1)*n+j;	
		Prob_1[k_k]=a_p*W_pr[k_k] + b_p*W_pr_new[k_k];						//u_2
		}				
	}


calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, Prob_1, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, W_pr_new, i_mi, i_ma, j_mi, j_ma);			//u_2_1

a_p=0.3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
b_p=0.6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;

	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)
		{
		k_k=(i-1)*n+j;	
		W_pr_new[k_k]=a_p*W_pr[k_k] + b_p*W_pr_new[k_k];						//u_2
		}				
	}

delete [] Prob_1;		

return;	
}


//_____________________________________________________________________________________________________________________________________________________________
void R_K_2_order_3_steps(int m, double * x, double d_x, int n, double * p, double d_p, double * W_pr, double k_diff, double d_t, double * Force_full, double max_p, double max_force, int i_mi, int i_ma, int j_mi, int j_ma, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * W_pr_new)
{
//__________����� ����� �����, ����� ������� �������������� �������, ������� �������, �����������
	
int N_tot, i, j, k_k;
double a_q, b_q, c_q, d_q;

N_tot=m*n;
double *Prob_2 = new double [N_tot+1];			//������������� ������
double *Prob_3 = new double [N_tot+1];			//������������� ������


calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, W_pr, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, W_pr_new, i_mi, i_ma, j_mi, j_ma);			//u_1_1	

a_q=1./6.;
b_q=5./6.;
	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)
		{
		k_k=(i-1)*n+j;	
		Prob_2[k_k]=a_q*W_pr[k_k] + b_q*W_pr_new[k_k];						//u_2
		}				
	}	

calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, Prob_2, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, W_pr_new, i_mi, i_ma, j_mi, j_ma);			//u_2_2	

a_q=9./20.;
b_q=11./120.;
c_q=11./24.;
	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)
		{
		k_k=(i-1)*n+j;	
		Prob_3[k_k]=a_q*W_pr[k_k] + b_q*Prob_2[k_k] + c_q*W_pr_new[k_k];						//u_3
		}				
	}
calc_weno(d_t, d_x, d_p, k_diff, m, x, n, p, Prob_3, Force_full, max_p, max_force, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, W_pr_new, i_mi, i_ma, j_mi, j_ma);			//u_3_3	

a_q=77./275.;
b_q=78./275.;
c_q=4./55.;
d_q=4./11.;
	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)
		{
		k_k=(i-1)*n+j;	
		W_pr_new[k_k]=a_q*W_pr[k_k] + b_q*Prob_2[k_k] + c_q*Prob_3[k_k] + d_q*W_pr_new[k_k];						//u_new
		}				
	}

delete [] Prob_2;
delete [] Prob_3;

return;	
}


