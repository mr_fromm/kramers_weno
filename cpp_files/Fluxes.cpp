

void flux_poz_neg(bool flux_x_p, int m, double * x, int n, double * p, double * Force_full, double * prob_dens, double max_flux_deriv, int i_min, int i_max, int j_min, int j_max, double * Flux_poz, double * Flux_neg)
{
//________������� �����, ����� Lax-friedrichs splitting	
//______flux_x_p - ����� �� ���������� ��� ��������: true - �� ����������, false - �� ��������
//______x - ������ ��������� ����������� m, p - ������ ��������� ����������� n, Force_full - ������ ����, ����������� m, prob_dens - ��������� �����������
//______max_flux_deriv - ������������ ����������� �� ������ �� ��������� (�� ������): ������������ �������, ���� flux_x_p=true; ������������ ����, ���� flux_x_p=false
//_____________ i_min, i_max, j_min, j_max - ������� �������� ��������� �������
//_________Flux_poz, Flux_neg - ������������� � ������������� ������

int i, j, k_k;	
double fl, f_0;

if(flux_x_p)
{
	

	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)	
		{
		k_k=(i-1)*n+j;	
		f_0=prob_dens[k_k];		
		fl=p[j]*f_0;					//main flux
		Flux_poz[k_k]=0.5*(fl+max_flux_deriv*f_0);		//pozitiv monotone flux
		Flux_neg[k_k]=0.5*(fl-max_flux_deriv*f_0);		//negativ monotone flux
		}			
	}
	
	
}
else
{
	

	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)	
		{
		k_k=(i-1)*n+j;	
		f_0=prob_dens[k_k];		
		fl=Force_full[k_k]*f_0;					//main flux
		Flux_poz[k_k]=0.5*(fl+max_flux_deriv*f_0);		//pozitiv monotone flux
		Flux_neg[k_k]=0.5*(fl-max_flux_deriv*f_0);		//negativ monotone flux
		}			
	}	
	
}


	
return;	
	
}



void flux_construction(bool flux_x_p, int m, double * x, int n, double * p, double * Force_full, double * prob_dens, int i_min, int i_max, int j_min, int j_max, double * Flux_poz)
{
//________������� �����, ����� WENO-ROE
//______flux_x_p - ����� �� ���������� ��� ��������: true - �� ����������, false - �� ��������
//______x - ������ ��������� ����������� m, p - ������ ��������� ����������� n, Force_full - ������ ����, ����������� m, prob_dens - ��������� �����������
//______max_flux_deriv - ������������ ����������� �� ������ �� ��������� (�� ������): ������������ �������, ���� flux_x_p=true; ������������ ����, ���� flux_x_p=false
//_____________ i_min, i_max, j_min, j_max - ������� �������� ��������� �������
//_________Flux_poz - �����

int i, j, k_k;	
double fl, f_0;

if(flux_x_p)
{
	

	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)	
		{
		k_k=(i-1)*n+j;	
		f_0=prob_dens[k_k];		
		fl=p[j]*f_0;					//main flux
		Flux_poz[k_k]=fl;		
		}			
	}
	
	
}
else
{
	

	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)	
		{
		k_k=(i-1)*n+j;	
		f_0=prob_dens[k_k];		
		fl=Force_full[k_k]*f_0;					//main flux
		Flux_poz[k_k]=fl;

		}			
	}	
	
}

	
return;	
	
}
