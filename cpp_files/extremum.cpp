using namespace std;
#include <iostream>
#include <cmath> 


double Potential_calc(bool branch, double k_new, double p_new, double hz_new, double hx_new, double a_new, double x_x)
{
	//___________________________����������� ���� � ����� x_x
//________________	branch - ����� �����, branch=true - ������� (� ����� ���������), branch=false - ������ (� ����� �������� � ����� ����������)
//_____________k_new, p_new, hz_new, a_new, hx_new - ��������� ���������� ��� � ������� ��������, ����� ��� ������������	
double f_f;	

if(branch)		//������� �����
{
	
f_f= 0.5*k_new*x_x*x_x -p_new*x_x + 0.5*sqrt( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) ) ;		
	
}	
else			//������ �����
{
	
f_f= 0.5*k_new*x_x*x_x -p_new*x_x - 0.5*sqrt( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) ) ;		
		
}
	
return(f_f);	
}



double Force_calc(bool branch, double k_new, double p_new, double hz_new, double hx_new, double a_new, double x_x)
{
	//___________________________����������� ���� � ����� x_x
//________________	branch - ����� �����, branch=true - ������� (� ����� ���������), branch=false - ������ (� ����� �������� � ����� ����������)
//_____________k_new, p_new, hz_new, a_new, hx_new - ��������� ���������� ��� � ������� ��������, ����� ��� ������������	
double f_f;	

if(branch)		//������� �����
{
	
f_f=( p_new - k_new*x_x + 0.5*a_new*(hz_new-a_new*x_x)/sqrt( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) ) );		
	
}	
else			//������ �����
{
	
f_f=( p_new - k_new*x_x - 0.5*a_new*(hz_new-a_new*x_x)/sqrt( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) ) );		
		
}
	
return(f_f);	
}


double second_deriv(bool branch, double k_new, double p_new, double hz_new, double hx_new, double a_new, double x_x)
{
	//_______________������ ����������� ���������� � ����� x_x, ����� ��� ����������� ���� ����� ����������
//________________	branch - ����� �����, branch=true - ������� (� ����� ���������), branch=false - ������ (� ����� �������� � ����� ����������)
//_____________k_new, p_new, hz_new, a_new, hx_new - ��������� ���������� ��� � ������� ��������, ����� ��� ������������
	
double f_f;	

if(branch)		//������� �����
{
	
f_f=( k_new + (0.5*a_new*a_new*hx_new*hx_new)/( ( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) )*sqrt( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) ) ) );		
	
}	
else			//������ �����
{
	
f_f=( k_new - (0.5*a_new*a_new*hx_new*hx_new)/( ( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) )*sqrt( hx_new*hx_new + (hz_new-a_new*x_x)*(hz_new-a_new*x_x) ) ) );	
		
}
	
return(f_f);		
}


double solve_eq(bool branch, double k_new, double p_new, double hz_new, double hx_new, double a_new, double x_1, double f_1, double x_2, double f_2, bool & type, bool & exist)
{
	//__________________type - ��� ����������: true - max, false - min
	//____________________exist - ���������� �� ��������� ��� ���
double epsil;	
double x_x, check, f_x, ch_1, ch_2;
epsil=1.e-6;

	while(abs(x_1-x_2)>epsil)	
	{
	x_x=(x_1+x_2)/2.;

	f_x=Force_calc(branch, k_new, p_new, hz_new, hx_new, a_new, x_x);	
	ch_1=f_1*f_x;
	ch_2=f_2*f_x;
	
		if(ch_1<=0.)
		{
		x_2=x_x;
		f_2=f_x;	
		}
	
		if(ch_2<=0.)
		{
		x_1=x_x;
		f_1=f_x;	
		}
	
	}

	x_x=(x_1+x_2)/2.;

			exist=true;				//��������� ����
check=second_deriv(branch, k_new, p_new, hz_new, hx_new, a_new, x_x);
	if(check<0)	type=true;			//max
	if(check>0)	type=false;			//min
	if(abs(check)<epsil)	
	{
			exist=false;			//���������� ���
			return(0.);			
	}

return(x_x);
	
}

void search_extr(bool branch, double k_new, double p_new, double hz_new, double hx_new, double a_new, double x_min, double x_max, int N_dim, int & n_min, double * Q_min, int & n_max, double * Q_max)
{
//________________	branch - ����� �����, branch=true - ������� (� ����� ���������), branch=false - ������ (� ����� �������� � ����� ����������)
//_____________����������� � ������������ ������� ��� ������ 	x_min, x_max
//_____________N_dim - ���-�� ��������� �������	N_dim
//_____________Q_min - ������ ����� ��������, ����������� n_min
//_____________Q_max - ������ ����� ���������, ����������� n_max
double epsil;
double d_x, x_it, x_1, x_2, f_1, f_2, f_prob, x_x;
bool exist, type;
epsil=1.e-6;
	
	d_x=(x_max-x_min)/(N_dim-1);
	
	x_it=x_min;
	
	n_min=0;
	n_max=0;
	while((x_it<=x_max)||(abs(x_it-x_max)<epsil))
	{
	x_1=x_it;
	x_2=x_it+d_x;
	f_1=Force_calc(branch, k_new, p_new, hz_new, hx_new, a_new, x_1);
	f_2=Force_calc(branch, k_new, p_new, hz_new, hx_new, a_new, x_2);
	
	f_prob=f_1*f_2;
		if(f_prob<=0.)
		{
		x_x=solve_eq(branch, k_new, p_new, hz_new, hx_new, a_new, x_1, f_1, x_2, f_2, type, exist);
			if(exist)	
			{
				if(type)
				{
				n_max=n_max+1;
				Q_max[n_max]=x_x;
				}
				else
				{
				n_min=n_min+1;
				Q_min[n_min]=x_x;	
				}
				
			}
			
		}
		
		
	x_it=x_it+d_x;	
	}
	
return;	
}

void trenie_constr(double gamm_1, double gamm_2, double mass, double alf, double x_sh, int m, double * x, double * gam)
{
//___________________������������ ������	
	//____________gamm_1, gamm_2 - ��������� ������ ����� � ������, alf - �������� ����������������, mass - �����
	//____________x - ������ ��������� ����������� m, gam - ������ ������ �������������
int i;
double x_x, g_g;

	for(i=1; i<=m; i++)	
	{
	x_x=x[i];
		if(x_x<=x_sh)	
		{
		g_g=gamm_1*mass/alf;
		}
		else
		{
		g_g=gamm_2*mass/alf;	
		}
	gam[i]=g_g;			//������������ ������
	}

return;	
}


double Force_u(bool branch, double k_new, double p_new, double hz_new, double hx_new, double a_new, int i_min, int i_max, int j_min, int j_max, double ga_1, double ga_2, double mass, double alf, int m, double * x, int n, double * p, double * Force, int N_dim, int & n_min, double * Q_min, int & n_max, double * Q_max)
{
//________________	branch - ����� �����, branch=true - ������� (� ����� ���������), branch=false - ������ (� ����� �������� � ����� ����������)
//_____________k_new, p_new, hz_new, a_new, hx_new - ��������� ���������� ��� � ������� ��������, ����� ��� ������������
//_______________Force - ��������� ������ ��� (������������� + ������) - ������� ���������� ��� ������������
//_____________ i_min, i_max, j_min, j_max - ������� �������� ��������� �������	(���� ���������� �� 1..m, 1..n - ����� �������� ����� ��������� �����)
//_____________x - ������ ��������� ����������� m, p - ������ ��������� ����������� n (��� ���������� ���� ��� ������������)
//______________gamm - ��������� ������, ������ ����������� m, �.�. ������ �� ���������� �������
//____________N_dim - ���-�� ��������� ������� ��� ������ ���������
//___________Q_min - ������ � ���������� ����������, �� ����� n_min
//___________Q_min - ������ � ����������� ����������, �� ����� n_max
//____________ga_1, ga_2 - ��������� ������ ����� � ������, alf - �������� ����������������, mass - �����
	
double f_f, f_tr, x_x, p_p, g_0, x_sh;
double x_min, x_max;
int k_k, i, j;

//_________________���������
x_min=x[i_min];
x_max=x[i_max];

search_extr(branch, k_new, p_new, hz_new, hx_new, a_new, x_min, x_max, N_dim, n_min, Q_min, n_max, Q_max);

if((n_min==0)&&(n_max==0))	cout << "Potential is monotone \n  ";
if(n_max>1)		cout << "More than 1 maximum in potential \n  ";
if((branch)&&(n_min>1))	cout << "Upper branch, more than 1 minimum \n  ";
if((!branch)&&(n_max==0)&&(n_min>1))	cout << "Lower branch, no maximum but more than 1 minimum \n  ";
//__________________������������ gamm - ������
      switch ( branch )  
      {  
         case true:  		//������� �����, ���� �������

 		x_sh=Q_min[n_min];	        
            break;  
         case false:  		//������ �����

		if(n_max==0)
		{			
		x_sh=Q_min[n_min];	
		}
		else
		{			
		x_sh=Q_max[n_max];		
		}

            break;  

      }  

double *gamm = new double [m+1];

trenie_constr(ga_1, ga_2, mass, alf, x_sh, m, x, gamm);

//___________������ ����
	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)	
		{
k_k=(i-1)*n+j;		
x_x=x[i];
p_p=p[j];	
g_0=gamm[i];
f_f=Force_calc(branch, k_new, p_new, hz_new, hx_new, a_new, x_x);	
f_tr=-g_0*p_p;		

Force[k_k]=f_f+f_tr;

		}			
	}

delete [] gamm;

return(x_sh);	
}

