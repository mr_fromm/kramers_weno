/*
��������� �������

*/
using namespace std;
#include <cmath> 


void geom(double t_1, double t_2, int m, double * arr, int i_mi, int i_ma, double & delt_t)
{
//________���������� ������, ��������� � 1, m - ���-�� ������ ����� � ������� (������ ����������� m+1)
//_______������� ������ ���������
//_______i_mi, i_ma - ������� �������� �������
int i;

delt_t=(t_2-t_1)/(i_ma-i_mi);	
	for(i=i_mi; i<=i_ma; i++)
	{
	arr[i]=t_1 + (i-i_mi)*delt_t;		
	}

delt_t=arr[i_mi+1]-arr[i_mi];
	for(i=(i_mi-1); i>=1; i--)
	{
	arr[i]=arr[i+1]-delt_t;
	}

delt_t=arr[i_ma]-arr[i_ma-1];
	for(i=(i_ma+1); i<=m; i++)
	{
	arr[i]=arr[i-1]+delt_t;		
	}

return;	
}


void init_cond(int m, double * x, int n, double * p, double c_x, double c_p, double * init_c, int i_mi, int i_ma, int j_mi, int j_ma)
{

//_____________������ ��������� �������
//_____________��������� ������ ������������ � ����������: i - �������, j -��������, ��������� � 1, ����������� m*n
//____________������� (i,j) � ������� ������������ ��� (i-1)*n+j, n - ����� ��������
//_____________ m - ����� ��������� ���������������� ����������
//_____________n - 	����� ��������� �� ��������
//______________c_x, c_p - ��������� ����� � ������ �������
//__________��� ���������� � ��������� ������� ���� ������ ��������, ���� �� ���������������� �����, �.� �� 1 �� m, �� 1 �� n

int i, j, k_k;
double x_0, p_0;
double sigm_p, sigm_x, g_1, g_2, epsil, pii, a_h_x, a_h_p, d_x, d_p, koef_e;
epsil=1.0-4;
pii=3.14159265358979323846264338327;
d_x=x[i_mi+1]-x[i_mi];
d_p=p[j_mi+1]-p[j_mi];

/* 
sigm_p=epsil;
sigm_x=epsil;
*/

koef_e=7.;
sigm_p=d_p*koef_e;
sigm_x=d_x*koef_e;

a_h_x=1./(sigm_x*sqrt(pii));
a_h_p=1./(sigm_p*sqrt(pii));
	for(i=1; i<=m; i++)
	{
		for(j=1; j<=n; j++)
		{
		k_k=(i-1)*n+j;	
		x_0=(x[i] - c_x)/sigm_x;
		p_0=(p[j] - c_p)/sigm_p;
		
		g_1=a_h_x*exp( -x_0*x_0 );
		g_2=a_h_p*exp( -p_0*p_0 );
		init_c[k_k]=g_1*g_2;					//������������� ������-�������

		}
		
	}

	
return;	
}
