using namespace std;
#include <cmath> 

//________________________������������� ����������
double weno_5_integral(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5)
{
	//______________�������� �� i �� i+1 �����
//_________________������������� ���� 5 ���������	�� ����� ������
//_________________����� �����������
//__����� �� �������: k-2, k-1, k, k+1, k+2	
//___________________(f1,  f2, f3, f4,  f5)


double alf_1, alf_2, alf_3;
double chis, chis_1;
double h_1, h_2, h_3;
double bet_1, bet_2, bet_3;
double gamm_1, gamm_2, gamm_3;
double omeg_precalc_1, omeg_precalc_2, omeg_precalc_3, summ_omeg;
double weight_1, weight_2, weight_3;
double epsil_e, weno_app;

epsil_e=1.e-6;

chis=0.08333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;

//_____________	k-2, k-1, k
alf_1=0.416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=-1.3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=1.916666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_1=alf_1*f_1+alf_2*f_2+alf_3*f_3;
bet_1=chis*( 25.*f_1*f_1 + 160.*f_2*f_2 - 196.*f_2*f_3 + 61.*f_3*f_3 + f_1*(-124.*f_2+74.*f_3) );

//_____________	k-1, k, k+1
alf_1=-0.0833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=0.666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_3=0.416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_2=alf_1*f_2+alf_2*f_3+alf_3*f_4;

bet_2=chis*( 13.*f_2*f_2 + 64.*f_3*f_3 - 76.*f_3*f_4 + 25.*f_4*f_4 + 26.*f_2*(-2.*f_3+f_4) );

//_____________k, k+1, k+2
alf_1=0.416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=0.666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_3=-0.0833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_3=alf_1*f_3+alf_2*f_4+alf_3*f_5;

bet_3=chis*( 25.*f_3*f_3 - 76.*f_3*f_4 + 64.*f_4*f_4 + 26.*f_5*(f_3-2.*f_4) + 13.*f_5*f_5 );

//____________weights
gamm_1=11./300.;
gamm_2=97./150.;
gamm_3=19./60.;

chis_1=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis_1*chis_1);

chis_1=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis_1*chis_1);

chis_1=epsil_e+bet_3;
omeg_precalc_3=gamm_3/(chis_1*chis_1);

summ_omeg=omeg_precalc_1+omeg_precalc_2+omeg_precalc_3;
weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;
weight_3=omeg_precalc_3/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2+weight_3*h_3;
weno_app=d_x*weno_app;

return(weno_app);
		
}


double Weno_3_integral(double d_x, double f_1, double f_2, double f_3)
{
		//______________�������� �� i �� i+1 �����
//_________________������������� ���� 3 ��������� �� ����� ������
//_________________����� �����������
//__����� �� �������: k-1, k, k+1
//___________________(f1,  f2, f3)

double alf_1, alf_2, chis;
double h_1, h_2, bet_1, bet_2;
double gamm_1, gamm_2, omeg_precalc_1, omeg_precalc_2, summ_omeg;
double weight_1, weight_2;
double weno_app, epsil_e;

epsil_e=1.e-6;	
//_____________	k-1, k	
alf_1=-0.5;
alf_2=1.5;
h_1=alf_1*f_1+alf_2*f_2;
chis=f_1-f_2;
bet_1=chis*chis;

//____________k, k+1
alf_1=0.5;
alf_2=0.5;
h_2=alf_1*f_2+alf_2*f_3;
chis=f_2-f_3;
bet_2=chis*chis;

//____________weights
gamm_1=0.1666666666666666666666666666666666666666666666666666666666666666666666666666666666;
gamm_2=0.8333333333333333333333333333333333333333333333333333333333333333333333333333333333;

chis=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis*chis);

chis=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis*chis);

summ_omeg=omeg_precalc_1+omeg_precalc_2;

weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2;
weno_app=weno_app*d_x;

return(weno_app);
	
}

//___________________________
double weno_5_integral_conserv(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5)
{
//________________________�������� �� i-0.5 �� i+0.5 �����	
	
//_________________������������� ���� 5 ���������	�� ����� ������
//_________________����� �����������
//__����� �� �������: k-2, k-1, k, k+1, k+2	
//___________________(f1,  f2, f3, f4,  f5)


double alf_1, alf_2, alf_3;
double chis, chis_1;
double h_1, h_2, h_3;
double bet_1, bet_2, bet_3;
double gamm_1, gamm_2, gamm_3;
double pre_ga_1_pl, pre_ga_2_pl, pre_ga_3_pl, pre_ga_1_mi, pre_ga_2_mi, pre_ga_3_mi;
double gamm_1_pl, gamm_2_pl, gamm_3_pl, gamm_1_mi, gamm_2_mi, gamm_3_mi;
double sigm_pl, sigm_mi, summ_w_pl, summ_w_mi, tet;
double omeg_pre_pl_1, omeg_pre_pl_2, omeg_pre_pl_3, omeg_pre_mi_1, omeg_pre_mi_2, omeg_pre_mi_3;
double omeg_1_pl, omeg_2_pl, omeg_3_pl, omeg_1_mi, omeg_2_mi, omeg_3_mi;
double R_pl, R_mi;
double epsil_e, weno_app;

epsil_e=1.e-6;


chis=0.3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;

//_____________	k-2, k-1, k
alf_1=0.041666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=-0.08333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=1.041666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_1=alf_1*f_1+alf_2*f_2+alf_3*f_3;
bet_1=chis*( 4.*f_1*f_1 - 19.*f_1*f_2 + 25.*f_2*f_2 + 11.*f_1*f_3 - 31.*f_2*f_3 + 10.*f_3*f_3 );

//_____________	k-1, k, k+1
alf_1=0.041666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=0.916666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_3=0.041666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_2=alf_1*f_2+alf_2*f_3+alf_3*f_4;
bet_2=chis*( 4.*f_2*f_2 - 13.*f_2*f_3 +13.*f_3*f_3 + 5.*f_2*f_4 - 13.*f_3*f_4 + 4.*f_4*f_4 );

//_____________k, k+1, k+2
alf_1=1.041666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=-0.08333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=0.041666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_3=alf_1*f_3+alf_2*f_4+alf_3*f_5;
bet_3=chis*( 10.*f_3*f_3 - 31.*f_3*f_4 + 25.*f_4*f_4 + 11.*f_3*f_5 - 19.*f_4*f_5 + 4.*f_5*f_5 );

//____________pozitiv & negativ weights
gamm_1=-0.0708333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
gamm_2=1.14166666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
gamm_3=-0.0708333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;

tet=3.;

pre_ga_1_pl=0.5*( gamm_1 + tet*abs(gamm_1) );
pre_ga_2_pl=0.5*( gamm_2 + tet*abs(gamm_2) );
pre_ga_3_pl=0.5*( gamm_3 + tet*abs(gamm_3) );
pre_ga_1_mi=pre_ga_1_pl-gamm_1;
pre_ga_2_mi=pre_ga_2_pl-gamm_2;
pre_ga_3_mi=pre_ga_3_pl-gamm_3;

sigm_pl=pre_ga_1_pl+pre_ga_2_pl+pre_ga_3_pl;
sigm_mi=pre_ga_1_mi+pre_ga_2_mi+pre_ga_3_mi;

gamm_1_pl=pre_ga_1_pl/sigm_pl;
gamm_2_pl=pre_ga_2_pl/sigm_pl;
gamm_3_pl=pre_ga_3_pl/sigm_pl;
gamm_1_mi=pre_ga_1_mi/sigm_mi;
gamm_2_mi=pre_ga_2_mi/sigm_mi;
gamm_3_mi=pre_ga_3_mi/sigm_mi;

chis_1=epsil_e+bet_1;
omeg_pre_pl_1=gamm_1_pl/( chis_1*chis_1 );
omeg_pre_mi_1=gamm_1_mi/( chis_1*chis_1 );

chis_1=epsil_e+bet_2;
omeg_pre_pl_2=gamm_2_pl/( chis_1*chis_1 );
omeg_pre_mi_2=gamm_2_mi/( chis_1*chis_1 );

chis_1=epsil_e+bet_3;
omeg_pre_pl_3=gamm_3_pl/( chis_1*chis_1 );
omeg_pre_mi_3=gamm_3_mi/( chis_1*chis_1 );

summ_w_pl=omeg_pre_pl_1+omeg_pre_pl_2+omeg_pre_pl_3;
summ_w_mi=omeg_pre_mi_1+omeg_pre_mi_2+omeg_pre_mi_3;

omeg_1_pl=omeg_pre_pl_1/summ_w_pl;
omeg_2_pl=omeg_pre_pl_2/summ_w_pl;
omeg_3_pl=omeg_pre_pl_3/summ_w_pl;
omeg_1_mi=omeg_pre_mi_1/summ_w_mi;
omeg_2_mi=omeg_pre_mi_2/summ_w_mi;
omeg_3_mi=omeg_pre_mi_3/summ_w_mi;

R_pl=omeg_1_pl*h_1 + omeg_2_pl*h_2 + omeg_3_pl*h_3;
R_mi=omeg_1_mi*h_1 + omeg_2_mi*h_2 + omeg_3_mi*h_3;

weno_app=R_pl*sigm_pl-R_mi*sigm_mi;
weno_app=d_x*weno_app;

return(weno_app);	
		
}


double weno_5_int_right(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5)
{
	//��������� �������� �� i �� i+0.5
	//________weno 5
//_________________����� �����������
//__����� �� �������: k-2, k-1, k, k+1, k+2	
//___________________(f1,  f2, f3, f4,  f5)


double alf_1, alf_2, alf_3;
double chis, chis_1;
double h_1, h_2, h_3;
double bet_1, bet_2, bet_3;
double gamm_1, gamm_2, gamm_3;
double omeg_precalc_1, omeg_precalc_2, omeg_precalc_3, summ_omeg;
double weight_1, weight_2, weight_3;
double epsil_e, weno_app;

epsil_e=1.e-6;

chis=0.0416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;

//_____________	k-2, k-1, k
alf_1=0.083333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=-0.291666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_3=0.708333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_1=alf_1*f_1+alf_2*f_2+alf_3*f_3;
bet_1=chis*( 19.*f_1*f_1 + 124.*f_2*f_2 - 154.*f_2*f_3 + 49.*f_3*f_3 + f_1*(-94.*f_2+56.*f_3) );

//_____________	k-1, k, k+1
alf_1=-0.0416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=0.458333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=0.083333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_2=alf_1*f_2+alf_2*f_3+alf_3*f_4;

bet_2=chis*( 13.*f_2*f_2 + 52.*f_3*f_3 - 58.*f_3*f_4 + 19.*f_4*f_4 + f_2*(-46.*f_3+20.*f_4) );

//_____________k, k+1, k+2
alf_1=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=0.208333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=-0.0416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_3=alf_1*f_3+alf_2*f_4+alf_3*f_5;

bet_3=chis*( 31.*f_3*f_3 - 94.*f_3*f_4 + 76.*f_4*f_4 + 2.*f_5*(16.*f_3-29.*f_4) + 13.*f_5*f_5 );

//____________weights
gamm_1=11./120.;
gamm_2=157./240.;
gamm_3=61./240.;

chis_1=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis_1*chis_1);

chis_1=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis_1*chis_1);

chis_1=epsil_e+bet_3;
omeg_precalc_3=gamm_3/(chis_1*chis_1);

summ_omeg=omeg_precalc_1+omeg_precalc_2+omeg_precalc_3;
weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;
weight_3=omeg_precalc_3/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2+weight_3*h_3;
weno_app=d_x*weno_app;

return(weno_app);	
		
}


double weno_5_int_left(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5)
{
	//��������� �������� �� i-0.5 �� i
	//________weno 5
//_________________����� �����������
//__����� �� �������: k-2, k-1, k, k+1, k+2	
//___________________(f1,  f2, f3, f4,  f5)


double alf_1, alf_2, alf_3;
double chis, chis_1;
double h_1, h_2, h_3;
double bet_1, bet_2, bet_3;
double gamm_1, gamm_2, gamm_3;
double omeg_precalc_1, omeg_precalc_2, omeg_precalc_3, summ_omeg;
double weight_1, weight_2, weight_3;
double epsil_e, weno_app;

epsil_e=1.e-6;

chis=0.0416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;

//_____________	k-2, k-1, k
alf_1=-0.0416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=0.208333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_1=alf_1*f_1+alf_2*f_2+alf_3*f_3;
bet_1=chis*( 13.*f_1*f_1 + 76.*f_2*f_2 - 94.*f_2*f_3 + 31.*f_3*f_3 + f_1*(-58.*f_2+32.*f_3) );

//_____________	k-1, k, k+1
alf_1=0.083333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=0.458333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=-0.0416666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_2=alf_1*f_2+alf_2*f_3+alf_3*f_4;

bet_2=chis*( 19.*f_2*f_2 + 52.*f_3*f_3 - 46.*f_3*f_4 + 13.*f_4*f_4 + f_2*(-58.*f_3+20.*f_4) );

//_____________k, k+1, k+2
alf_1=0.708333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=-0.291666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_3=0.083333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
h_3=alf_1*f_3+alf_2*f_4+alf_3*f_5;

bet_3=chis*( 49.*f_3*f_3 - 154.*f_3*f_4 + 124.*f_4*f_4 + 2.*f_5*(28.*f_3-47.*f_4) + 19.*f_5*f_5 );

//____________weights
gamm_1=61./240.;
gamm_2=157./240.;
gamm_3=11./120.;

chis_1=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis_1*chis_1);

chis_1=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis_1*chis_1);

chis_1=epsil_e+bet_3;
omeg_precalc_3=gamm_3/(chis_1*chis_1);

summ_omeg=omeg_precalc_1+omeg_precalc_2+omeg_precalc_3;
weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;
weight_3=omeg_precalc_3/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2+weight_3*h_3;
weno_app=d_x*weno_app;

return(weno_app);	
		
}

double rectangle_integr(double d_x, double f_1, double f_2)
{
	//________�������� �� ����� ������ ������� ������� ���������������
	//_______k,   k+1
	//_______f_1, f_2
	
double trapez;
	
trapez=0.5*(f_1+f_2)*d_x;

return(trapez);
	
}


double integr_calc(int n_control, double d_z, int m, double * F_z, int i_1, int i_2)
{
//_________________��������� ���������	
//__________________����� �����������
//_______n_control - ����������� ��������: =1 - ������� ����������, =2 - ������� �� weno 3, =3 - ������� �� weno 5, =4 - conservative weno5
//_______	F_z - ������ �������� �������, d_z - ��� �� �����������
//________i_1, i_2 - ������� �������������� � ������� z

double summ_in, integr;
double f_1, f_2, f_3, f_4, f_5;
int i;

summ_in=0.;
	


		switch(n_control)
		{		
		case 1:				//��������

	for(i=i_1; i<i_2; i++)	
	{		
		f_1=F_z[i];
		f_2=F_z[i+1];
		integr=rectangle_integr(d_z, f_1, f_2);
	summ_in=summ_in+integr;
	}
				
		break;
			
		case 2:				//Weno 3

	for(i=i_1; i<i_2; i++)	
	{		
		f_1=F_z[i-1];
		f_2=F_z[i];
		f_3=F_z[i+1];
		integr=Weno_3_integral(d_z, f_1, f_2, f_3);		
	summ_in=summ_in+integr;
	}
				
		break;	
	
		case 3:				//Weno 5

	for(i=i_1; i<i_2; i++)	
	{		
		f_1=F_z[i-2];
		f_2=F_z[i-1];
		f_3=F_z[i];
		f_4=F_z[i+1];
		f_5=F_z[i+2];
		integr=weno_5_integral(d_z, f_1, f_2, f_3, f_4, f_5);	
	summ_in=summ_in+integr;
	}
			
		break;	

		case 4:				//conservative Weno 5

//__________________�������� ���������� �� ������� �����
		f_1=F_z[i_1-2];
		f_2=F_z[i_1-1];
		f_3=F_z[i_1];
		f_4=F_z[i_1+1];
		f_5=F_z[i_1+2];
		integr=weno_5_int_right(d_z, f_1, f_2, f_3, f_4, f_5);		//�� i �� i+0.5
	summ_in=summ_in+integr;

		f_1=F_z[i_2-2];
		f_2=F_z[i_2-1];
		f_3=F_z[i_2];
		f_4=F_z[i_2+1];
		f_5=F_z[i_2+2];
		integr=weno_5_int_left(d_z, f_1, f_2, f_3, f_4, f_5);		//�� i-0.5 �� i
	summ_in=summ_in+integr;

//___________________main summ

	for(i=(i_1+1); i<=(i_2-1); i++)	
	{	
	//_______from i-0.5 up to i+0.5	
		f_1=F_z[i-2];
		f_2=F_z[i-1];
		f_3=F_z[i];
		f_4=F_z[i+1];
		f_5=F_z[i+2];
		integr=weno_5_integral_conserv(d_z, f_1, f_2, f_3, f_4, f_5);	
	summ_in=summ_in+integr;
	}
			
		break;	
				
		default:
		return(0.);
		break;	
		}
		

	
return(summ_in);
	
}

