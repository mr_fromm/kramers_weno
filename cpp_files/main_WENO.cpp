using namespace std;

#include "class_weno_1.h"

#include <iostream>
#include <fstream>

void massiv_init_with_0(int N_m, double * mass);
void work_file(char * name_f, int m, int n, double * mas, int i_mi, int i_ma, int j_mi, int j_ma);
void read_from_file(char * name, int n, double * koeffs);
double full_probavility(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma);

int main()
{
	int N_x=128, N_p=512;			//���-�� ����� �� �����
	int N_tot=N_x*N_p;
	int ord=2;						//������� �����: 1 - ������ �������, 2 - ������ �������
	bool bran=false,				//����� ����������, true - ������, false - ������
		 tun=false;					//���������� ��������, true - ��������, false - ���������
	//__________��������� �����: ����������� � ������������ �������� ����������, ��������
	double 	 x_1=-1.,
			 x_2=1.0,
			 p_1=-10.,
			 p_2=10.;
	//____________��������� �����
	double	 x_st,			//�� �	- ����������	
			 p_st;			//�� �	- �������
	double 	k_y, p_e, h_z, h_x, a_i, gamm_1, gamm_2, mass, korrel;			//��������� ����������, ������ � ���������� ��������� ���� (���������)
	//__________�������, ���������
	double P_open, P_close;			//����������� ��������� � ��������� ��������� � �������������
	double P_o[2], P_c[2];
//__________����� ������
char name_pot[20]="potent_param.txt";     
char name_geom[20]="geom_param.txt";  
char name_other[20]="other_param.txt";  
int N_pot=5,			//���-�� ���������� ����������
	N_geom=6,			//���-�� ���������� ���������
	N_other=4;			//������ ���������
double arr_pot[N_pot], arr_geom[N_geom], arr_other[N_other];			//������� ��� ����������
//______________________����� ��� ������
char name_x[20]="x.txt";     
char name_p[20]="p.txt";  
char name_prob_x[20]="Prob_ot_x.txt";  
char name_prob_p[20]="Prob_ot_p.txt";     
char name_prob[20]="Prob_x_p.txt";  
char name_open[20]="P_open.txt";  
char name_close[20]="P_close.txt";  

//______________������������ �������
double *x_r= new double [N_x+1];				//x
double *p_r= new double [N_p+1];				//p
double *Prob_r_on_x= new double [N_x+1];		//��������� ������ �� � � �������������
double *Prob_r_on_p= new double [N_p+1];		//�������� ������ �� � � �������������
double *Prob_r= new double [N_tot+1];				//��������� ����������� � �������������

//______________��������� ��������� ���������
read_from_file(name_geom, N_geom, arr_geom);

	x_1=arr_geom[0];
	x_2=arr_geom[1];
	p_1=arr_geom[2];
	p_2=arr_geom[3];
//________________��������� �����	(��� ������-�������)	
	x_st=arr_geom[4];			//�� �		
	p_st=arr_geom[5];			//�� �
	
//____________________���������		 
read_from_file(name_pot, N_pot, arr_pot);

	k_y=arr_pot[0];			//����������� ���������
	p_e=arr_pot[1];		//������� ��������
	h_z=arr_pot[2];		//�������������� ��������� 
	h_x=arr_pot[3];		//�������������� ��������� ��������������
	a_i=arr_pot[4];		//��������� �������������� �������� � ��������

read_from_file(name_other, N_other, arr_other);

	gamm_1=arr_other[0];		//�������� ������ ����� �� ���������� - ���������, ���� �� ���� (����� ����� - ������ �� ������� �����)
	gamm_2=arr_other[1];		//�������� ������ ������ �� ���������� - ���������, ���� �� ���� (����� ����� - ������ �� ������� �����)
	mass=arr_other[2];		//����� �������
	korrel=arr_other[3];		//�������������� ��������� ���� (����� ����� - ���� �� ������� �����)
//________________________
//________________�������������� �������
massiv_init_with_0(N_x, Prob_r_on_x);
massiv_init_with_0(N_p, Prob_r_on_p);
massiv_init_with_0(N_tot, Prob_r);

//__________________����� � �������

    class_WENO_first obj_Weno(ord, bran, tun, N_x, N_p, x_1, x_2, p_1, p_2, x_st, p_st, k_y, p_e, h_z, h_x, a_i, gamm_1, gamm_2, mass, korrel); 	// ��������� �������������; 
    obj_Weno.get_outputs(x_r, p_r, Prob_r, Prob_r_on_x, Prob_r_on_p, P_open, P_close);

//______________����� � ����, ������ ��� � �������������
work_file(name_x, 1, N_x, x_r, 1, 1, 1, N_x);				//������ �, ���������
work_file(name_p, 1, N_p, p_r, 1, 1, 1, N_p);				//������ p, ���������
work_file(name_prob_x, 1, N_x, Prob_r_on_x, 1, 1, 1, N_x);				//������ ����������� ������ �� �, ���������
work_file(name_prob_p, 1, N_p, Prob_r_on_p, 1, 1, 1, N_p);				//������ ����������� ������ �� �, ���������
work_file(name_prob, N_x, N_p, Prob_r, 1, N_x, 1, N_p);				//��������� ���������, �� ������� - ����������, �� �������� - ��������

P_o[1]=P_open;						//����������� ��������� ���������
P_c[1]=P_close;						//����������� ��������� ���������
//___________������ P_open � P_close � ����

work_file(name_open, 1, 1, P_o, 1, 1, 1, 1);		
work_file(name_close, 1, 1, P_c, 1, 1, 1, 1);

//double P_full=full_probavility(N_x, x_r, N_p, p_r, Prob_r, 1, N_x, 1, N_p);

//_____________������ �������
delete [] x_r;
delete [] p_r;
delete [] Prob_r_on_x;
delete [] Prob_r_on_p;
delete [] Prob_r;
	
return(0);	
}



void massiv_init_with_0(int N_m, double * mass)
{

int i;		
	for(i=0; i<=N_m; i++)
	{		
	mass[i]=0.;	
	}
	
return;		
	
}

 
void work_file(char * name_f, int m, int n, double * mas, int i_mi, int i_ma, int j_mi, int j_ma)
{
int i,j,k_k;  

ofstream g; 

		g.open (name_f, ios::out ); 
		
	for( i =i_mi; i<=i_ma; i++)
	{
		for( j =j_mi; j<=j_ma; j++)
		{
		k_k=(i-1)*n+j;	
		g<<mas[ k_k ]<< "                         \t ";		
		}
	g<<" \n ";	
	}	
g.close ( ); 

return;
}

void read_from_file(char * name, int n, double * koeffs)
{
	//____________������ �� ����� name ����� double, n- ���-�� ����������� ��������
int i;	
ifstream ifs;

        ifs.open(name, std::ios_base::binary);

        for (i = 0; i < n; i++){
             ifs >> koeffs[i];
                    }
        ifs.close();	
	
}

