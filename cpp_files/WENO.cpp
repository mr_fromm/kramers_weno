double Weno_5_flux_pozitiv(double f_1, double f_2, double f_3, double f_4, double f_5)
{
//_________________������������� ���� 5 �������������� ������	
//_________________����� �����������
//__����� �� �������: k-2, k-1, k, k+1, k+2	
//___________________(f1,  f2, f3, f4,  f5)
//__�������������� ������������� � ����� k+0.5

double alf_1, alf_2, alf_3;
double chis_1, chis_2;
double h_1, h_2, h_3;
double bet_1, bet_2, bet_3;
double gamm_1, gamm_2, gamm_3;
double omeg_precalc_1, omeg_precalc_2, omeg_precalc_3, summ_omeg;
double weight_1, weight_2, weight_3;
double epsil_e, weno_app;

epsil_e=1.e-6;
//_____________	k-2, k-1, k
alf_1=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=-1.1666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_3=1.833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_1=alf_1*f_1+alf_2*f_2+alf_3*f_3;
chis_1=f_1-2.*f_2+f_3;
chis_2=f_1-4.*f_2+3.*f_3;
bet_1=13./12.*chis_1*chis_1 + 1./4.*chis_2*chis_2;

//_____________	k-1, k, k+1
alf_1=-0.1666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=0.833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_2=alf_1*f_2+alf_2*f_3+alf_3*f_4;
chis_1=f_2-2.*f_3+f_4;
chis_2=f_2-f_4;
bet_2=13./12.*chis_1*chis_1 + 1./4.*chis_2*chis_2;

//_____________k, k+1, k+2
alf_1=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=0.833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=-0.1666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_3=alf_1*f_3+alf_2*f_4+alf_3*f_5;
chis_1=f_3-2.*f_4+f_5;
chis_2=3.*f_3-4.*f_4+f_5;
bet_3=13./12.*chis_1*chis_1 + 1./4.*chis_2*chis_2;

//____________weights
gamm_1=0.1;
gamm_2=0.6;
gamm_3=0.3;

chis_1=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis_1*chis_1);

chis_1=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis_1*chis_1);

chis_1=epsil_e+bet_3;
omeg_precalc_3=gamm_3/(chis_1*chis_1);

summ_omeg=omeg_precalc_1+omeg_precalc_2+omeg_precalc_3;
weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;
weight_3=omeg_precalc_3/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2+weight_3*h_3;

return(weno_app);
	
}



double Weno_5_flux_negativ(double f_1, double f_2, double f_3, double f_4, double f_5)
{
//_________________������������� ���� 5 �������������� ������	
//_________________����� �����������
//__����� �� �������: k-1, k, k+1, k+2, k+3	
//___________________(f1,  f2, f3, f4,  f5)
//__�������������� ������������� � ����� k+0.5


double alf_1, alf_2, alf_3;
double chis_1, chis_2;
double h_1, h_2, h_3;
double bet_1, bet_2, bet_3;
double gamm_1, gamm_2, gamm_3;
double omeg_precalc_1, omeg_precalc_2, omeg_precalc_3, summ_omeg;
double weight_1, weight_2, weight_3;
double epsil_e, weno_app;

epsil_e=1.e-6;
//_____________	k-1, k, k+1
alf_1=-0.1666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_2=0.833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_1=alf_1*f_1+alf_2*f_2+alf_3*f_3;
chis_1=f_1-2.*f_2+f_3;
chis_2=f_1-f_3;
bet_1=13./12.*chis_1*chis_1 + 1./4.*chis_2*chis_2;

//_____________	k, k+1, k+2
alf_1=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=0.833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_3=-0.1666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;	
h_2=alf_1*f_2+alf_2*f_3+alf_3*f_4;
chis_1=f_2-2.*f_3+f_4;
chis_2=3.*f_2-4.*f_3+f_4;
bet_2=13./12.*chis_1*chis_1 + 1./4.*chis_2*chis_2;

//_____________k+1, k+2, k+3
alf_1=1.833333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;
alf_2=-1.1666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666;
alf_3=0.333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333;	
h_3=alf_1*f_3+alf_2*f_4+alf_3*f_5;
chis_1=f_3-2.*f_4+f_5;
chis_2=5.*f_3-8.*f_4+3.*f_5;
bet_3=13./12.*chis_1*chis_1 + 1./4.*chis_2*chis_2;

//____________weights
gamm_1=0.3;
gamm_2=0.6;
gamm_3=0.1;

chis_1=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis_1*chis_1);

chis_1=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis_1*chis_1);

chis_1=epsil_e+bet_3;
omeg_precalc_3=gamm_3/(chis_1*chis_1);

summ_omeg=omeg_precalc_1+omeg_precalc_2+omeg_precalc_3;
weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;
weight_3=omeg_precalc_3/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2+weight_3*h_3;

return(weno_app);
	
}


double Weno_3_flux_pozitiv(double f_1, double f_2, double f_3)
{
//_________________������������� ���� 3 �������������� ������	
//_________________����� �����������
//__����� �� �������: k-1, k, k+1
//___________________(f1,  f2, f3)
//__�������������� ������������� � ����� k+0.5

double alf_1, alf_2, chis;
double h_1, h_2, bet_1, bet_2;
double gamm_1, gamm_2, omeg_precalc_1, omeg_precalc_2, summ_omeg;
double weight_1, weight_2;
double weno_app, epsil_e;

epsil_e=1.e-6;	
//_____________	k-1, k	
alf_1=-0.5;
alf_2=1.5;
h_1=alf_1*f_1+alf_2*f_2;
chis=f_1-f_2;
bet_1=chis*chis;

//____________k, k+1
alf_1=0.5;
alf_2=0.5;
h_2=alf_1*f_2+alf_2*f_3;
chis=f_2-f_3;
bet_2=chis*chis;

//____________weights
gamm_1=0.3333333333333333333333333333333333333333333333333333333333333333333333333333333333;
gamm_2=0.6666666666666666666666666666666666666666666666666666666666666666666666666666666666;

chis=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis*chis);

chis=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis*chis);

summ_omeg=omeg_precalc_1+omeg_precalc_2;

weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2;

return(weno_app);
	
}

double Weno_3_flux_negativ(double f_1, double f_2, double f_3)
{
//_________________������������� ���� 3 �������������� ������	
//_________________����� �����������
//__����� �� �������: k, k+1, k+2
//___________________(f1,  f2, f3)
//__�������������� ������������� � ����� k+0.5

double alf_1, alf_2, chis;
double h_1, h_2, bet_1, bet_2;
double gamm_1, gamm_2, omeg_precalc_1, omeg_precalc_2, summ_omeg;
double weight_1, weight_2;
double weno_app, epsil_e;

epsil_e=1.e-6;	

//____________k, k+1
alf_1=0.5;
alf_2=0.5;
h_1=alf_1*f_1+alf_2*f_2;
chis=f_1-f_2;
bet_1=chis*chis;

//____________k+1, k+2
alf_1=1.5;
alf_2=-0.5;
h_2=alf_1*f_2+alf_2*f_3;
chis=f_2-f_3;
bet_2=chis*chis;

//____________weights
gamm_1=0.6666666666666666666666666666666666666666666666666666666666666666666666666666666666;
gamm_2=0.3333333333333333333333333333333333333333333333333333333333333333333333333333333333;

chis=epsil_e+bet_1;
omeg_precalc_1=gamm_1/(chis*chis);

chis=epsil_e+bet_2;
omeg_precalc_2=gamm_2/(chis*chis);

summ_omeg=omeg_precalc_1+omeg_precalc_2;

weight_1=omeg_precalc_1/summ_omeg;
weight_2=omeg_precalc_2/summ_omeg;

weno_app=weight_1*h_1+weight_2*h_2;

return(weno_app);
	
}


