#include "class_weno_1.h"


class_WENO_first::class_WENO_first(int ord, bool bran, bool tun, int N_1, int N_2, double x_1, double x_2, double p_1, double p_2, double x_s, double p_s, double k, double p, double h_d, double h_o, double a, double ga_1, double ga_2, double ma, double ko) 
{
set_param(ord, bran, tun, N_1, N_2, x_1, x_2, p_1, p_2, x_s, p_s, k, p, h_d, h_o, a, ga_1, ga_2, ma, ko) ; 		// параметры устанавливаем
}
 
//_______________________параметры
void class_WENO_first::set_param(int ord, bool bran, bool tun, int N_1, int N_2, double x_1, double x_2, double p_1, double p_2, double x_s, double p_s, double k, double p, double h_d, double h_o, double a, double ga_1, double ga_2, double ma, double ko) 
{
 order=ord; 	
 branch=bran; 	
 tunnel=tun; 	
 N_x=N_1;
 N_p=N_2;
 x_min=x_1;
 x_max=x_2;
 p_min=p_1;
 p_max=p_2;
 x_start=x_s;
 p_start=p_s;
 k_yp=k;
 p_exter=p;
 hz=h_d;
 hx=h_o;
 a_in=a;
 gamm_1=ga_1;
 gamm_2=ga_2;
 mass=ma;
 korrel=ko;
}

bool class_WENO_first::get_outputs(double * x_r, double * p_r, double * Prob_r, double * Prob_r_on_x, double * Prob_r_on_p, double & P_open, double & P_close)
{
bool y_n;

y_n=main_method(order, branch, tunnel, korrel, mass, gamm_1, gamm_2, k_yp, p_exter, hz, hx, a_in, x_min, x_max, p_min, p_max, N_x, N_p, x_start, p_start, x_r, p_r, Prob_r, Prob_r_on_x, Prob_r_on_p, P_open, P_close);

	return(y_n);	
}



