
using namespace std;
#include <cmath> 
/*
����� ���� �� �������, ������ �� ����������

*/

double max_search_2(int m, int n, double * F_arr, int i_min, int i_max, int j_min, int j_max)
{
//_________���� �������� � ��������� ������� � ���������: 	i_min, i_max; j_min, j_max
int i, j, k_k;	
double max, pr;

k_k=(i_min-1)*n+j_min;
max=abs(F_arr[k_k]);

	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)	
		{
			
		k_k=(i-1)*n+j;
		pr=abs(F_arr[k_k]);
			if(pr>max)
			{
			max=pr;				
			}

		}			
	}

return(max);	
}


double max_search_1(int m,  double * F_arr, int i_min, int i_max)
{
//_________���� �������� � ���������� ������� � ���������: 	i_min, i_max; j_min, j_max
int i;	
double max, pr;

i=i_min;
max=abs(F_arr[i]);

	for(i=1; i<=m; i++)	
	{
			
		pr=abs(F_arr[i]);
			if(pr>max)
			{
			max=pr;				
			}
			
	}

return(max);	
}


double time_step(double d_x_min, double d_p_min, double k_diff, int n, double * p, int m, double * Force_f, double & max_p, double & max_F)
{
	
	/*
	������� ���������� ���
	d_x, d_p - ����������� ���� �� ���������� � �������� (���� ����� �������������)
	k_diff - ����������� ��� ������ ����������� �� ��������� � ��������
	p - ������ ������ ���������, ���-�� n, ��������� � 1
	F - ������ ������ ���, ��������� (m*n)
	*/
	
double dt, dt_1, dt_2, dt_3, c_prob, norm_f_p, norm_p, a_h;
int i,j, k_k;

c_prob=0.9;									//��������������� �����, ������ ���� ������ 1

max_p=max_search_1(n, p, 1, n);
max_F=max_search_2(m, n, Force_f, 1, m, 1, n);

dt_1=0.25*d_x_min/( max_p );
dt_2=0.25*d_p_min/( max_F );
dt_3=0.25*(d_p_min*d_p_min)/(k_diff);

	if(dt_1<dt_2)
	{
	dt=dt_1;
	}
	else
	{
	dt=dt_2;	
	}

	if(dt<dt_3)
	{
	dt=dt;
	}
	else
	{
	dt=dt_3;	
	}

dt=c_prob*dt;
return(dt);	
}

