using namespace std;
#include <iostream>
#include <cmath> 
//_______________���������
void key_param(bool branch, double korrel, double mass, double gamm_1, double gamm_2, double * old_par, double * new_par, double & t_0, double & x_0, double & p_0, double & W_0, double & U_0, double & k_diff, double & alf);
void geom(double t_1, double t_2, int m, double * arr, int i_mi, int i_ma, double & delt_t);
double Force_u(bool branch, double k_new, double p_new, double hz_new, double hx_new, double a_new, int i_min, int i_max, int j_min, int j_max, double ga_1, double ga_2, double mass, double alf, int m, double * x, int n, double * p, double * Force, int N_dim, int & n_min, double * Q_min, int & n_max, double * Q_max);
void init_cond(int m, double * x, int n, double * p, double c_x, double c_p, double * init_c, int i_mi, int i_ma, int j_mi, int j_ma);
void R_K_2_order_2_steps(int m, double * x, double d_x, int n, double * p, double d_p, double * W_pr, double k_diff, double d_t, double * Force_full, double max_p, double max_force, int i_mi, int i_ma, int j_mi, int j_ma, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * W_pr_new);
void R_K_3_order_3_steps(int m, double * x, double d_x, int n, double * p, double d_p, double * W_pr, double k_diff, double d_t, double * Force_full, double max_p, double max_force, int i_mi, int i_ma, int j_mi, int j_ma, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * W_pr_new);
void R_K_2_order_3_steps(int m, double * x, double d_x, int n, double * p, double d_p, double * W_pr, double k_diff, double d_t, double * Force_full, double max_p, double max_force, int i_mi, int i_ma, int j_mi, int j_ma, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * W_pr_new);
double time_step(double d_x_min, double d_p_min, double k_diff, int n, double * p, int m, double * Force_f, double & max_p, double & max_F);
void calc_inter(int m, double * x,  int n, double * p, double * F, double * prob_dens_n, double k_diff, double d_t, double * prob_dens_n_pl_1, int i_min, int i_max, int j_min, int j_max);
void weno_x_p(bool type, int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma, double * dens_z);
void density_x(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma, double * dens_x);
void density_p(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma, double * dens_p);
double integr_rectangle(double z_1, double z_2, int m, double * z, double * f_z);
void extrapol_1_dim_2(int m, double * z, double * f_z, int i_mi, int i_ma);
void calc_weno(double d_t, double d_x, double d_p, double k_diff, int m, double * x, int n, double * p, double * prob_dens_old, double * Force_full, double max_p, double max_force, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * prob_dens_new, int i_min, int i_max, int j_min, int j_max);
double full_probavility(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma);
//void work_file(char name, int m, int n, double * mas, int i_mi, int i_ma, int j_mi, int j_ma);

//______________��� ����������
double mass_copy(int m, int n, double * arr_1, double * arr_2)
{
//_______________������������ ������ arr_2 � arr_1, arr_1 ��������
int i, j, k_k;
double ph_1, ph_2, epsil, epsil_max;

epsil_max=0.;
	for(i=1; i<=m; i++)	
	{
		for(j=1; j<=n; j++)	
		{
		k_k=(i-1)*n+j;
		ph_1=arr_1[k_k];
		ph_2=arr_2[k_k];
		epsil=abs(ph_2-ph_1);
		if(epsil>epsil_max)	epsil_max=epsil;			//������������ ��������
		
		//_____________������ � ��������
		arr_1[k_k]=ph_2;
		arr_2[k_k]=0.;

		}			
	}	

return(epsil_max);	
}

void const_multiply_1(int m, int i_mi, int i_ma, int k_f, double razm, double * arr, double * arr_new)
{
	//___________��� ����������� �������
//_______________��������� �� ��������� ��������� razm
//_______________i_mi..i_ma - �������� ������� � ������� arr
//_______________arr_new - ������ ������� �����������, �������� ��� ��������� ��������
//_______________k_f - ����� ��������� �����

int i, i_i;	
	
	for(i=i_mi; i<=i_ma; i++)	
	{
		i_i=i-k_f;
	arr_new[i_i]=razm*arr[i];
	}
	
return;	
}

void const_multiply_2(int m, int n, int i_mi, int i_ma, int j_mi, int j_ma, int k_f, double razm, double * arr, int m_new, int n_new, double * arr_new)
{
	//___________��� ����������� �������
//_______________��������� �� ��������� ��������� razm
//_______________i_mi..i_ma - �������� ������� � ������� arr (m*n)
//_______________arr_new - ������ ������� ����������� (m_new*n_new), �������� ��� ��������� ��������
//_______________k_f - ����� ��������� �����

int i, j, i_i, j_j, k_k, k_k_new;	
	
	for(i=i_mi; i<=i_ma; i++)	
	{
		for(j=j_mi; j<=j_ma; j++)	
		{
			
		i_i=i-k_f;
		j_j=j-k_f;
		k_k=(i-1)*n+j;
		k_k_new=(i_i-1)*n_new+j_j;
		
	arr_new[k_k_new]=razm*arr[k_k];			
		}		
	}
	
return;	
}

bool main_method(int order, bool branch, bool tunnel, double korrel, double mass, double gamm_1, double gamm_2, double k_old, double p_old, double hz_old, double hx_old, double a_old, double x_min, double x_max, double p_min, double p_max, int N_x, int N_p, double st_x, double st_p, double * x_r, double * p_r, double * Prob_r, double * Prob_r_on_x, double * Prob_r_on_p, double & P_open, double & P_close)
{
	//____order - ������� �����: 1 - ������, 2 - ������
//_______ branch - ����� �����, branch=true - ������� (� ����� ���������), branch=false - ������ (� ����� �������� � ����� ����������)
//________tunnel - ������������ �� ����������, true - �������, false - �� �������
//________ k_old, p_old, hz_old, hx_old, a_old - ��������� ��������� ���������� (����������� ��� � ������� � ��������)
//________ k_new, p_new, hz_new, hx_new, a_new - ������������ ��������� ����������
//_______korrel - ���������� ��������� ���� (���������� � ������� �����) � ������ ������� �������
//_____ x_0, p_o, t_0, U_0, W_0 - ����������� ������� ����������, ��������, �������, ����������, ��������� �����������
//_______alf, gamm_1, gamm_2 - ��������� ������ ��� �������� � �������� (�� � ������� ����)
//________x_min, x_max, p_min, p_max - �������� ����� (���������), N_x, N_p - ����� ����� �� �����
//_______k_diff - ����������� ��� ������ ����������� � ��������
//_______x_r, p_r, Prob_r - ��������� ������� ���������, ���������, ��������� ����������� (��� ��������� �������)
//________st_x, st_p - �����, � ������ �������� (��� ������-�������)
//__________Prob_r_on_x, Prob_r_on_p - ��������� �� ���������� (�� �), �������� (�� �) �������������

int k_f=3;						//�������������� ��������� �����
int i_min=k_f+1;				//������ �������� ������� �� �
int i_max=N_x+k_f;				//����� �������� ������� �� �
int j_min=k_f+1;				//������ �������� ������� �� �
int j_max=N_p+k_f;				//����� �������� ������� �� �
int N_p_full=N_p+2*k_f;			//������ ����� ��������� ����� �� � (�������� + ���������)
int N_x_full=N_x+2*k_f;			//������ ����� ��������� ����� �� � (�������� + ���������)
int N_tot=N_p_full*N_x_full;		//��� ����������� �������

bool stat_sol, type;
double k_new, p_new, hz_new, hx_new, a_new, t_0, x_0, p_0, W_0, W_0_p, W_0_x, U_0, k_diff, alf;
double x_new_mi, x_new_ma, p_new_mi, p_new_ma;
double d_t, d_x_n, d_p_n, st_x_new, st_p_new, d_x_r, d_p_r;
double max_p, max_F;			//������������ ������� � ����
double x_sh;					//�����, ����������� high open & low open
double time;
int n_min, n_max, i, N_iter;
int N_iter_max=50000;			//������������ ���-�� ��������
int N_vivod=100;				//������ ������� �������� ������� �����
double epsil_it=1.e-6;			//�������� ��� ������ �������������
double eps_now;
double P_full;	

int N_dim=20;					//���-�� ��������� ������� ��� ������ ����������	
double Q_min[N_dim+1];
double Q_max[N_dim+1];

double old_param[5];
double new_param[5];

double *x= new double [N_x_full+1];
double *p= new double [N_p_full+1];
double *Prob_x= new double [N_x_full+1];
double *Prob_p= new double [N_p_full+1];
double *Force_f= new double [N_tot+1];
double *Prob_dens_old= new double [N_tot+1];
double *Prob_dens_new= new double [N_tot+1];
double *flux_x_poz= new double [N_tot+1];
double *flux_x_neg= new double [N_tot+1];
double *flux_p_poz= new double [N_tot+1];
double *flux_p_neg= new double [N_tot+1];


old_param[0]=k_old;
old_param[1]=p_old;
old_param[2]=hz_old;
old_param[3]=hx_old;
old_param[4]=a_old;
key_param(branch, korrel, mass, gamm_1, gamm_2, old_param, new_param, t_0, x_0, p_0, W_0, U_0, k_diff, alf);			//�������� ������������ ��������� � ��������� ��� �������� � ��������� ���������

k_new=new_param[0];
p_new=new_param[1];
hz_new=new_param[2];
hx_new=new_param[3];
a_new=new_param[4];
//___________________������������ ����������
x_new_mi=x_min/x_0;
x_new_ma=x_max/x_0;
p_new_mi=p_min/p_0;
p_new_ma=p_max/p_0;
st_x_new=st_x/x_0;
st_p_new=st_p/p_0;
//____________________

//________________��� ���������� ����������

W_0_p=1./p_0;
W_0_x=1./x_0;

//_________________________�����
//_______________������������
geom(x_new_mi, x_new_ma, N_x_full, x, i_min, i_max, d_x_n);
geom(p_new_mi, p_new_ma, N_p_full, p, j_min, j_max, d_p_n);

//_________________________________________������ ���������� ����������, ������ ���� (�������������+������)
//______________�������������� �������
for(i=0; i<=N_dim; i++)	
{
Q_min[i]=0.;
Q_max[i]=0.;	
}

x_sh=Force_u(branch, k_new, p_new, hz_new, hx_new, a_new, i_min, i_max, j_min, j_max, gamm_1, gamm_2, mass, alf, N_x_full, x, N_p_full, p, Force_f, N_dim, n_min, Q_min, n_max, Q_max);

//______________________________� ������ �������������� ��������
switch ( branch )  
{
	case true:			//�������, ���� �������
	
		if(st_x_new<x_sh)	cout << "Upper branch, start from opened state \n  ";
		else cout << "Upper branch, start from closed state \n  ";
		
		
	break;
	
	case false:			//������ �����
	
		if(n_max>0)	cout << "Lower branch, maximum exist \n  ";
		if(st_x_new<x_sh)	cout << "Lower branch, start from closed state \n  ";
		else cout << "Lower branch, start from opened state  \n  ";		
					
	break;		
}

//______________________________________��������� ������� �������� (������ �������������� ��������� ��������!!!)
init_cond(N_x_full, x, N_p_full, p, st_x_new, st_p_new, Prob_dens_old, i_min, i_max, j_min, j_max);

//_______________________________��� �� �������
	d_t=time_step(d_x_n, d_p_n, k_diff, N_p_full, p, N_x_full, Force_f, max_p, max_F);

cout <<"\n steps: "<<" d_x= "<<d_x_n<<" d_p= "<<d_p_n<<" d_t= "<<d_t<<" \n ";
//______________________________�������
time=0.;
N_iter=0;
eps_now=1.;
cout <<" \n ";
while((eps_now>epsil_it)&&(N_iter<=N_iter_max))
{



	switch ( order )  
	{
		case 1:			//������ �������
calc_inter(N_x_full, x,  N_p_full, p, Force_f, Prob_dens_old, k_diff, d_t, Prob_dens_new, i_min, i_max, j_min, j_max);


		
		case 2:			//������ ������� �� ������� � �, �
//R_K_2_order_2_steps(N_x_full, x, d_x_n, N_p_full, p, d_p_n, Prob_dens_old, k_diff, d_t, Force_f, max_p, max_F, i_min, i_max, j_min, j_max, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, Prob_dens_new);
//R_K_3_order_3_steps(N_x_full, x, d_x_n, N_p_full, p, d_p_n, Prob_dens_old, k_diff, d_t, Force_f, max_p, max_F, i_min, i_max, j_min, j_max, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, Prob_dens_new);
//R_K_2_order_3_steps(N_x_full, x, d_x_n, N_p_full, p, d_p_n, Prob_dens_old, k_diff, d_t, Force_f, max_p, max_F, i_min, i_max, j_min, j_max, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, Prob_dens_new);
calc_weno(d_t, d_x_n, d_p_n, k_diff, N_x_full, x, N_p_full, p, Prob_dens_old, Force_f, max_p, max_F, flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg, Prob_dens_new, i_min, i_max, j_min, j_max);
	
		break;
		
		default:
		
		return(false);
		break;	
		
	}
	
eps_now=mass_copy(N_x_full, N_p_full, Prob_dens_old, Prob_dens_new);
if(  ((N_iter/N_vivod)*N_vivod)==N_iter  )	
{
//	cout <<" current time = "<<(time*t_0)<<"; progress to stationary "<<(epsil_it/eps_now*100)<<"% \n ";			//������� ���������
cout <<"progress to stationary "<<(epsil_it/eps_now*100)<<"%  ; ";			//������� ���������
	
P_full=full_probavility(N_x_full, x, N_p_full, p, Prob_dens_old, i_min, i_max, j_min, j_max);
cout << " Full probability  =  "<<P_full<< "\n";	
}


time=time+d_t;
N_iter=N_iter+1;
}

if(N_iter<=N_iter_max)	
stat_sol=true;				//����� ������������
else
stat_sol=false;				//�� ����� ������������


//__________________________��������� �� ��������� � ���������� �� �����������

	switch ( order )  
	{
		case 1:			//������ �������
density_x(N_x_full, x, N_p_full, p, Prob_dens_old, i_min, i_max, j_min, j_max, Prob_x);			//��������� �� �
density_p(N_x_full, x, N_p_full, p, Prob_dens_old, i_min, i_max, j_min, j_max, Prob_p);			//��������� �� p		
		break;
		
		case 2:			//������ ������� �� ������� � �, �
type=true;
weno_x_p(type, N_x_full, x, N_p_full, p, Prob_dens_old, i_min, i_max, j_min, j_max, Prob_x);		//��������� �� �
type=false;
weno_x_p(type, N_x_full, x, N_p_full, p, Prob_dens_old, i_min, i_max, j_min, j_max, Prob_p);		//��������� �� p		
		break;
		
		default:
		
		return(false);
		break;	
		
	}


extrapol_1_dim_2(N_x_full, x, Prob_x, i_min, i_max);

if(branch)
{
P_open=integr_rectangle(x[i_min], x_sh, N_x_full, x, Prob_x);	
P_close=integr_rectangle( x_sh, x[i_max], N_x_full, x, Prob_x);
//P_close=1.-P_open;
}
else
{
P_open=integr_rectangle(x_sh, x[i_max], N_x_full, x, Prob_x);	
P_close=integr_rectangle(x[i_min], x_sh, N_x_full, x, Prob_x);
//P_close=1.-P_open;	
}

//_______________________������� � ��������� �������
const_multiply_1(N_x_full, i_min, i_max, k_f, x_0, x, x_r);
const_multiply_1(N_p_full, j_min, j_max, k_f, p_0, p, p_r);
const_multiply_2(N_x_full, N_p_full, i_min, i_max, j_min, j_max, k_f, W_0, Prob_dens_old, N_x, N_p, Prob_r);
const_multiply_1(N_x_full, i_min, i_max, k_f, W_0_x, Prob_x, Prob_r_on_x);
const_multiply_1(N_p_full, j_min, j_max, k_f, W_0_p, Prob_p, Prob_r_on_p);



delete [] x;
delete [] p;
delete [] Prob_x;
delete [] Prob_p;
delete [] Force_f;
delete [] Prob_dens_old;
delete [] Prob_dens_new;
delete [] flux_x_poz;
delete [] flux_x_neg;
delete [] flux_p_poz;
delete [] flux_p_neg;

return(stat_sol);	
}
