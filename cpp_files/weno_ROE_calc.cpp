
using namespace std;
#include <cmath> 


void extension_arr(int m, double * x, int n, double * p, double * arr, int i_mi, int i_ma, int j_mi, int j_ma);
void flux_poz_neg(bool flux_x_p, int m, double * x, int n, double * p, double * Force_full, double * prob_dens, double max_flux_deriv, int i_min, int i_max, int j_min, int j_max, double * Flux_poz, double * Flux_neg);
double Weno_5_flux_pozitiv(double f_1, double f_2, double f_3, double f_4, double f_5);
double Weno_5_flux_negativ(double f_1, double f_2, double f_3, double f_4, double f_5);
void flux_construction(bool flux_x_p, int m, double * x, int n, double * p, double * Force_full, double * prob_dens, int i_min, int i_max, int j_min, int j_max, double * Flux_poz);


void boundary_cond(int m, int n, double * W_pr_new, int i_mi, int i_ma, int j_mi, int j_ma)
{
//________W_pr_new - ������, � ������� ������ ��������� �������
double 	F_p_1, F_p_2, F_x_1, F_x_2;
int i,j, k_k;
//______________________���������� ��������� ������� �������

	F_p_1=0.;			//�������� ��� p=pl
	F_p_2=0.;			//�������� ��� p=pr
	F_x_1=0.;			//�������� ��� x=xl
	F_x_2=0.;			//�������� ��� x=xr
	
	//____________��������� ������� �� �������	
	for(i=1; i<=m; i++)
	{
	j=j_mi;									//p=pl, ����� ����� �������
	k_k=(i-1)*n+j;
	W_pr_new[k_k]=F_p_1;
	
	j=j_ma;									//p=pr, ����� ������� �������
	k_k=(i-1)*n+j;
	W_pr_new[k_k]=F_p_2;	
	}

	//____________��������� ������� �� ����������
	for(j=1; j<=n; j++)
	{
	i=i_mi;									//x=xl, ����� ����� ����������	
	k_k=(i-1)*n+j;
	W_pr_new[k_k]=F_x_1;		
	
	i=i_ma;									//x=xr, ����� ������� ����������	
	k_k=(i-1)*n+j;
	W_pr_new[k_k]=F_x_2;
			
	}

//__________________________________________________________

return;
	
}

void calc_weno(double d_t, double d_x, double d_p, double k_diff, int m, double * x, int n, double * p, double * prob_dens_old, double * Force_full, double max_p, double max_force, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * prob_dens_new, int i_min, int i_max, int j_min, int j_max)
{
//___________________global flux splitting, weno reconstruction
//_______________���������� �� 1 ��� �� �������
//____________d_t, d_x, d_p - ���� �� �������, ����������, �������� (����� �����������)
//__________x  ������ ��������� m, p - ������ ��������� n
//__________Force_full - ������ ���� (m*n) = �������������+������
//___________prob_dens_old - ��������� �� ���������� ���� �� �������
//___________max_p, max_force - ������������ ����������� �� ������� �� ��������� (����� ���, ����� �� �)
//________flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg - ������ �� � � �, ����� ����������� ����������� Lax-Fridriechs (+ � -)
//_________prob_dens_new - ��������� � ��������� ������ �������
//_________i_min,  i_max,  j_min,  j_max - ������� �������� ��������� �������, �� ���� - ��������� �����

int i, j, k_k, k_0, k_1, k_2, k_3, k_4, k_5;
double f_1, f_2, f_3, f_4, f_5, f_0, conserve_x, conserve_p, monotone_diff;
double hh_1, hh_2, pozitiv, negativ, f_f_new;
bool flux_x_p;

//____________________________pozitiv & negative fluxex construction, Lax-Friedrics splitting
flux_x_p=true;	
flux_poz_neg(flux_x_p, m, x, n, p, Force_full, prob_dens_old, max_p, 1, m, 1, n, flux_x_poz, flux_x_neg);
flux_x_p=false;	
flux_poz_neg(flux_x_p, m, x, n, p, Force_full, prob_dens_old, max_force, 1, m, 1, n, flux_p_poz, flux_p_neg);	


	for(i=(i_min+1); i<=(i_max-1); i++)	
	{
		for(j=(j_min+1); j<=(j_max-1); j++)	
		{

		//_____________fluxex
		//_____________�� x

		//_____________i+0.5		
		//__________________pozitiv

		k_0=i;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_poz[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_poz[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_poz[k_k];		

		pozitiv=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
		
		//____________________negativ
		
		k_0=i;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_neg[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_neg[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_neg[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_neg[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_neg[k_k];		

		negativ=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);	
		
		hh_2=pozitiv+negativ;
						
		//_____________i-0.5		
		//__________________pozitiv

		k_0=i-1;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_poz[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_poz[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_poz[k_k];		

		pozitiv=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
		
		//____________________negativ
		
		k_0=i-1;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_neg[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_neg[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_neg[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_neg[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_neg[k_k];		

		negativ=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);	
		
		hh_1=pozitiv+negativ;
		
		//______________________conserve derivative
		conserve_x=(hh_2-hh_1)/d_x;
		
		//_____________�� p

		//_____________j+0.5		
		//__________________pozitiv		

		k_0=j;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_poz[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_poz[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_poz[k_k];		

		pozitiv=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
		
		//____________________negativ
		
		k_0=j;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_neg[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_neg[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_neg[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_neg[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_neg[k_k];		

		negativ=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);	
		
		hh_2=pozitiv+negativ;
		

		//_____________j-0.5		
		//__________________pozitiv		

		k_0=j-1;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_poz[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_poz[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_poz[k_k];		

		pozitiv=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
		
		//____________________negativ
		
		k_0=j-1;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_neg[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_neg[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_neg[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_neg[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_neg[k_k];		

		negativ=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);	
		
		hh_1=pozitiv+negativ;
		
		//______________________conserve derivative
		conserve_p=(hh_2-hh_1)/d_p;
		
		//______________________���������� ��������
		k_0=j;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;

		k_k=(i-1)*n+k_1;
		f_1=prob_dens_old[k_k];
		k_k=(i-1)*n+k_2;
		f_2=prob_dens_old[k_k];
		k_k=(i-1)*n+k_3;
		f_3=prob_dens_old[k_k];
		
		monotone_diff=k_diff*(f_1-2.*f_2+f_3)/(d_p*d_p);
		
		//______________________ direct update
		k_k=(i-1)*n+j;
		f_0=prob_dens_old[k_k];
		f_f_new=f_0+d_t*(-conserve_x-conserve_p+monotone_diff);

		prob_dens_new[k_k]=f_f_new;

			
		}			
	}

//____________________boundary condition
boundary_cond(m, n, prob_dens_new, i_min, i_max, j_min, j_max);

//____________________extention
extension_arr(m, x, n, p, prob_dens_new, i_min, i_max, j_min, j_max);			//������������� � ��������� �������

return;	
}





void calc_weno_ROE(double d_t, double d_x, double d_p, double k_diff, int m, double * x, int n, double * p, double * prob_dens_old, double * Force_full, double max_p, double max_force, double * flux_x_poz, double * flux_x_neg, double * flux_p_poz, double * flux_p_neg, double * prob_dens_new, int i_min, int i_max, int j_min, int j_max)
{
//___________________���������� �� 1 ��� �� �������
//______________local upwinding, ROE speed, weno reconstruction
//____________d_t, d_x, d_p - ���� �� �������, ����������, �������� (����� �����������)
//__________x  ������ ��������� ����������� m, p - ������ ��������� ����������� n
//__________Force_full - ������ ���� (m*n) =�������������+������
//___________prob_dens_old - ��������� ����������� �� ������� ����
//___________max_p, max_force - ������������ ������� � ���� �� ������ (����� ��� ���������� + � - �������)
//________flux_x_poz, flux_x_neg, flux_p_poz, flux_p_neg - ������ �� ����������� � �������� (+ � +)
//_________prob_dens_new - ��������� �� ����� ����
//_________i_min,  i_max,  j_min,  j_max - ������� �������� ������� �� �����

int i, j, k_k, k_0, k_1, k_2, k_3, k_4, k_5;
int i_m, j_m;
double f_1, f_2, f_3, f_4, f_5, f_0, conserve_x, conserve_p, monotone_diff;
double hh_1, hh_2, pozitiv, negativ, f_f_new;
bool flux_x_p;

flux_x_p=true;
flux_construction(flux_x_p, m, x, n, p, Force_full, prob_dens_old, i_min, i_max, j_min, j_max, flux_x_poz);
flux_x_p=false;
flux_construction(flux_x_p, m, x, n, p, Force_full, prob_dens_old, i_min, i_max, j_min, j_max, flux_p_poz);


	for(i=(i_min+1); i<=(i_max-1); i++)	
	{
		for(j=(j_min+1); j<=(j_max-1); j++)	
		{

		//_____________fluxex
		//_____________�� �

		//_____________i+0.5		
		k_1=i;
		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		f_3=prob_dens_old[k_k];
		
		k_2=i+1;		
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		f_4=prob_dens_old[k_k];

pozitiv=(f_2-f_1)/(f_4-f_3);			//Roe speed

if(pozitiv>0.)
{
	//_______________left approxim
	
		k_0=i;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_poz[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_poz[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_poz[k_k];		

		hh_2=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
	
}
else
{
//________________right approxim	
	
		k_0=i;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_poz[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_poz[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_poz[k_k];		

		hh_2=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);	
	
}
	
					
		//_____________i-0.5		
		k_1=i-1;
		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		f_3=prob_dens_old[k_k];
		
		k_2=i;		
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		f_4=prob_dens_old[k_k];

pozitiv=(f_2-f_1)/(f_4-f_3);			//Roe speed

if(pozitiv>0.)
{
	//_______________left approxim
	
		k_0=i-1;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_poz[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_poz[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_poz[k_k];		

		hh_1=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
	
}
else
{
//________________right approxim	
	
		k_0=i-1;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(k_1-1)*n+j;
		f_1=flux_x_poz[k_k];
		k_k=(k_2-1)*n+j;
		f_2=flux_x_poz[k_k];
		k_k=(k_3-1)*n+j;
		f_3=flux_x_poz[k_k];
		k_k=(k_4-1)*n+j;
		f_4=flux_x_poz[k_k];
		k_k=(k_5-1)*n+j;
		f_5=flux_x_poz[k_k];		

		hh_1=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);	
	
}

		
		//______________________conserve derivative
		conserve_x=(hh_2-hh_1)/d_x;
		
		//_____________�� p

		//_____________j+0.5		

		k_1=j;
		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		f_3=prob_dens_old[k_k];
		
		k_2=j+1;		
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		f_4=prob_dens_old[k_k];

pozitiv=(f_2-f_1)/(f_4-f_3);			//Roe speed

if(pozitiv>0.)
{
	//_______________left approxim
	

		k_0=j;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_poz[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_poz[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_poz[k_k];		

		hh_2=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
	
}
else
{
//________________right approxim	
	
		k_0=j;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_poz[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_poz[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_poz[k_k];		

		hh_2=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);
	
}
	

		//_____________j-0.5		

		k_1=j-1;
		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		f_3=prob_dens_old[k_k];
		
		k_2=j;		
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		f_4=prob_dens_old[k_k];

pozitiv=(f_2-f_1)/(f_4-f_3);			//Roe speed


if(pozitiv>0.)
{
	//_______________left approxim
	
		k_0=j-1;
		
		k_1=k_0-2;
		k_2=k_0-1;
		k_3=k_0;
		k_4=k_0+1;
		k_5=k_0+2;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_poz[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_poz[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_poz[k_k];		

		hh_1=Weno_5_flux_pozitiv(f_1, f_2, f_3, f_4, f_5);
	
}
else
{
//________________right approxim	

		k_0=j-1;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;
		k_4=k_0+2;
		k_5=k_0+3;

		k_k=(i-1)*n+k_1;
		f_1=flux_p_poz[k_k];
		k_k=(i-1)*n+k_2;
		f_2=flux_p_poz[k_k];
		k_k=(i-1)*n+k_3;
		f_3=flux_p_poz[k_k];
		k_k=(i-1)*n+k_4;
		f_4=flux_p_poz[k_k];
		k_k=(i-1)*n+k_5;
		f_5=flux_p_poz[k_k];		

		hh_1=Weno_5_flux_negativ(f_1, f_2, f_3, f_4, f_5);		

	
}
		
		//______________________conserve derivative
		conserve_p=(hh_2-hh_1)/d_p;
		
		//______________________�������� �� ���������
		k_0=j;
		
		k_1=k_0-1;
		k_2=k_0;
		k_3=k_0+1;

		k_k=(i-1)*n+k_1;
		f_1=prob_dens_old[k_k];
		k_k=(i-1)*n+k_2;
		f_2=prob_dens_old[k_k];
		k_k=(i-1)*n+k_3;
		f_3=prob_dens_old[k_k];
		
		monotone_diff=k_diff*(f_1-2.*f_2+f_3)/(d_p*d_p);
		
		//______________________ direct update
		k_k=(i-1)*n+j;
		f_0=prob_dens_old[k_k];
		f_f_new=f_0+d_t*(-conserve_x-conserve_p+monotone_diff);

		prob_dens_new[k_k]=f_f_new;
		
		}			
	}

//____________________boundary condition
boundary_cond(m, n, prob_dens_new, i_min, i_max, j_min, j_max);

//____________________extention
extension_arr(m, x, n, p, prob_dens_new, i_min, i_max, j_min, j_max);			//������� �������

return;	
}


//_______________________������ �������
void calc_inter(int m, double * x,  int n, double * p, double * F, double * prob_dens_n, double k_diff, double d_t, double * prob_dens_n_pl_1, int i_min, int i_max, int j_min, int j_max)
{
//____________________����� ������� �������, upwind �� �, central diffrences �� �
//___________________m - ����� ��������� � ������� �, n - ����� ��������� � ������� p
//_________________�� � �� 	1 �� m-1, �� � �� 1 �� n-1 - � ���� ������ ��� �������������
//__________________��� ��������� ����� - � ��������� ��������
//____________________prob_dens_n - m*n? ��������� ����������� �� ���� n
//__________________F - ������ ����, ���-�� ����� ��� � � �
//_________________d_t - ������ ��� �� �������
//______________prob_dens_n_pl_1 - ��������� ����������� �� ���� n+1


int i, k, k_k, k_k_1, k_k_2, i_m, j_m;
double deriv_x, deriv_p_1, deriv_p_2, f_0;
double x_1, x_2, x_0, f_1, f_2, p_1, p_2, p_0, Force, f_calc;
double max_res, res, time_next;
double f_t_1, f_t_0;
double a_h, b_h, c_h, d_h, g_h, d_p_p;
double time, f_non_hom;

d_p_p=p[j_min+1]-p[j_min];
	for(i=(i_min+1); i<=(i_max-1); i++)
	{
		for(k=(j_min+1); k<=(j_max-1); k++)
		{
			
			k_k=(i-1)*n+k;
			f_0=prob_dens_n[k_k];
			p_0=p[k];
			
			if(p_0>0)					//�������� ������ ������
			{
			k_k_1=(i-1-1)*n+k; 
			k_k_2=(i-1)*n+k;
			x_1=x[i-1];
			x_2=x[i];
			f_1=prob_dens_n[k_k_1];
			f_2=prob_dens_n[k_k_2];
			deriv_x=(f_1-f_2)/(x_1-x_2);
			}
			else
			{
			k_k_1=(i-1)*n+k; 
			k_k_2=(i-1+1)*n+k;
			x_1=x[i];
			x_2=x[i+1];
			f_1=prob_dens_n[k_k_1];
			f_2=prob_dens_n[k_k_2];	
			deriv_x=(f_2-f_1)/(x_2-x_1);			
			}
										//_______________
			k_k_1=(i-1)*n+k-1; 
			k_k_2=(i-1)*n+k+1;			
			p_1=p[k-1];
			p_2=p[k+1];
			f_1=prob_dens_n[k_k_1];
			f_2=prob_dens_n[k_k_2];	

			deriv_p_2=(f_1-2.*f_0+f_2)/(d_p_p*d_p_p);		//������ ����������� �� �

/*
			Force_0=F[k_k];
			k_k_1=(i-1)*n+k-1; 
			k_k_2=(i-1)*n+k+1;			
			p_1=p[k-1];
			p_2=p[k+1];
			f_1=prob_dens_n[k_k_1];
			f_2=prob_dens_n[k_k_2];	

			deriv_p_1=(f_2-f_1)/(p_2-p_1);			//����������� ����������� �� �
								


			
			f_calc=f_0 + d_t*( -p_0*deriv_x - Force_0*deriv_p_1 + k_diff*deriv_p_2 + f_0 );
*/
			k_k_1=(i-1)*n+k-1; 
			k_k_2=(i-1)*n+k+1;			
			p_1=p[k-1];
			p_2=p[k+1];
			f_1=prob_dens_n[k_k_1]*F[k_k_1];
			f_2=prob_dens_n[k_k_2]*F[k_k_2];	

			deriv_p_1=(f_2-f_1)/(p_2-p_1);			//����������� ����������� �� �


			f_calc=f_0 + d_t*( -p_0*deriv_x - deriv_p_1 + k_diff*deriv_p_2);
			prob_dens_n_pl_1[k_k]=f_calc;
					
		}
		
	}

//____________________boundary condition
boundary_cond(m, n, prob_dens_n_pl_1, i_min, i_max, j_min, j_max);

//_____________extention
extension_arr(m, x, n, p, prob_dens_n_pl_1, i_min, i_max, j_min, j_max);			//������� �������

return;	
}


