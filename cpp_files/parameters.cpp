using namespace std;
#include <cmath> 

double trenie_p(double gamm_1, double gamm_2, double mass)
{
//____________________����������� �������� ������ ��� ��������� � ����������� �� �������� (���������)
//____________gamm_1, gamm_2 - ������ � ��������� � ����������� �� �������� (�� ����������: gamm_1 - �����, gamm_2 - ������), ���������
//_____________mass - ����� �������
double gamm, alf;	
	
if(gamm_2>gamm_1)	
{
	gamm=gamm_2;
}
else
{
	gamm=gamm_1;	
}

alf=gamm*mass;

return(alf);
	
}


void key_param(bool branch, double korrel, double mass, double gamm_1, double gamm_2, double * old_par, double * new_par, double & t_0, double & x_0, double & p_0, double & W_0, double & U_0, double & k_diff, double & alf)
{
//________________������������ �������� ��������� ��������� ��� �������� � ������������ ���������
//________________����� ������������ ������������ ��������� ��� ������������� ����������
//_______ branch - ����� �����, branch=true - ������� (� ����� ���������), branch=false - ������ (� ����� �������� � ����� ����������)
//________ k_old, p_old, hz_old, hx_old, a_old - ��������� ��������� ���������� (����������� ��� � ������� � ��������)
//________ k_new, p_new, hz_new, hx_new, a_new - ������������ ��������� ����������
//_______korrel - ���������� ��������� ���� (���������� � ������� �����) � ������ ������� �������
//_____ x_0, p_o, t_0, U_0, W_0 - ����������� ������� ����������, ��������, �������, ����������, ��������� �����������
//_______alf, gamm_1, gamm_2 - ��������� ������ ��� �������� � �������� (�� � ������� ����)
//_______k_diff - ����������� ��� ������ ����������� � ��������

double k_new, p_new, hz_new, hx_new, a_new;
double k_old, p_old, hz_old, hx_old, a_old;

k_old=old_par[0];
p_old=old_par[1];
hz_old=old_par[2]; 
hx_old=old_par[3]; 
a_old=old_par[4];

alf=trenie_p(gamm_1, gamm_2, mass);
	
if(branch)			//������� �����
{
U_0=abs( (-4.*hz_old*k_old + (a_old+2.*p_old)*(a_old+2.*p_old) )/(8.*k_old) );	
}
else				//������ �����
{
U_0=abs( (4.*hz_old*k_old + (a_old-2.*p_old)*(a_old-2.*p_old) )/(8.*k_old) );		
}

t_0=mass/alf;
p_0=sqrt(mass*U_0);
x_0=p_0/alf;
W_0=1./(x_0*p_0);

k_new=k_old*x_0*x_0/U_0;
p_new=p_old*x_0/U_0;
hz_new=hz_old/U_0;
hx_new=hx_old/U_0;
a_new=a_old*x_0/U_0;

k_diff=0.5*(korrel*mass*mass)/(alf*U_0);
//k_diff=(korrel*mass*mass)/(alf*U_0);

new_par[0]=k_new;
new_par[1]=p_new;
new_par[2]=hz_new;
new_par[3]=hx_new;
new_par[4]=a_new;

return;
}


