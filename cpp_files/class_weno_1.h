// ������������ ���� ������
bool main_method(int order, bool branch, bool tunnel, double korrel, double mass, double gamm_1, double gamm_2, double k_old, double p_old, double hz_old, double hx_old, double a_old, double x_min, double x_max, double p_min, double p_max, int N_x, int N_p, double st_x, double st_p, double * x_r, double * p_r, double * Prob_r, double * Prob_r_on_x, double * Prob_r_on_p, double & P_open, double & P_close);

class class_WENO_first 
{
private: 
	//_____________��������� ��������
	int order;				//����� ������ �������: 1 - ������� ������� �� ������� � �����������, 2 - WENO, ������ ������� �� ������� � �����������
	bool branch,			//����� ����� ����������: true- ������� (���� �������), false - ������ (��� ��������, ���� ��������)
		 tunnel;			//�������� �� ���������� ��������: true - ��������, false - ���������
	//_____________��������� �����
    double  x_min, 			//����������� ���������� �� �, ���������
        	x_max, 			//������������ ���������� �� �, ���������
        	p_min, 			//����������� ���������� �� �, ���������
			p_max; 			//������������ ���������� �� �, ���������
	int		N_x,			//����� ����� �� �
			N_p;			//����� ����� �� �
	
	//_____________��������� �� ������		
	double	k_yp, 			//����������� ���������, ���������
			p_exter, 			//������� �������� � �� ������, ���������
			hz, 			//����������� ��� ������� ����� z, ���������
			hx, 			//����������� ��� ������� ����� x, ���������
			a_in, 			//����������� �������������� ����������� � ��������������� ���������, ���������
			gamm_1, 			//������ ����� �� ����� ����������, ���������	(����������� ������ �� ������� �����)
			gamm_2, 			//������ ������ �� ����� ����������, ���������	(����������� ������ �� ������� �����)
			mass, 			//����� �������, ���������
			korrel; 			//�������������� ��������� ���� (���� �� ������� ����� �(t)=F(t)/m)
	
	//___________������ �������� (��������� ���������, ��� ������-�������)
	double  p_start, 			//��������� �������, ���������
			x_start;	 			//��������� ����������, ���������	 
			
			
public: 
    class_WENO_first(int, bool, bool, int, int, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double); 					
	void set_param(int, bool, bool, int, int, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double); 		// ��������� �������������
	bool get_outputs(double *, double *, double *, double *, double *, double &, double &);						//��������� ��������
}; 


