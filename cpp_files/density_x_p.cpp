using namespace std;
#include <cmath> 

double weno_5_integral(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5);
double Weno_3_integral(double d_x, double f_1, double f_2, double f_3);
void extrapol_1_dim_2(int m, double * z, double * f_z, int i_mi, int i_ma);
double integr_calc(int n_control, double d_z, int m, double * F_z, int i_1, int i_2);
double weno_5_integral_conserv(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5);
double weno_5_int_right(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5);
double weno_5_int_left(double d_x, double f_1, double f_2, double f_3, double f_4, double f_5);
//___________________________Weno

double liner_interpol(double x_1, double f_1, double x_2, double f_2, double x_s)
{
	//____________�������� ������������
	double f_s;
	
f_s=f_1 + (x_s-x_1)*(f_2-f_1)/(x_2-x_1);
return(f_s);	
}


//______________������������ ��������
double integr_rectangle(double z_1, double z_2, int m, double * z, double * f_z)
{
//______________������������ �������� �� 	z_1 �� z_2 �� ������� f_z, z - ������ �������
int i, k_1, k_2;
double 	epsil, z_z, ch, f_1, f_2, d_z, S_0, f_s, x_1, x_2;

epsil=1.e-6;

S_0=0.;
	//___________������ ������� ��������� �����
	for(i=1; i<=m; i++)
	{
	z_z=z[i];	

		if(z_z>z_1)	
		{
		k_1=i;
		break;		
		}
		
	}

ch=abs(z[k_1-1]-z_1);
	if(ch<epsil)	
	{
	k_1=k_1-1;		
	}
	else
	{
	x_1=z[k_1-1];
	f_1=f_z[k_1-1];
	x_2=z[k_1];
	f_2=f_z[k_1];		
	f_s=liner_interpol(x_1, f_1, x_2, f_2, z_1);	
	
	d_z=(x_2-z_1);
	z_z=0.5*( f_s+f_2 )*d_z;
	S_0=S_0+z_z;		
	}


	for(i=1; i<=m; i++)
	{
	z_z=z[i];	

		if(z_z>z_2)	
		{
		k_2=i;	
		break;	
		}
		
	}	

ch=abs(z[k_2-1]-z_2);
	if(ch<epsil)	
	{
	k_2=k_2-1;	
	
	d_z=z[k_1+1]-z[k_1];
	z_z=0.5*( f_z[k_2-1]+f_z[k_2] )*d_z;
	S_0=S_0+z_z;		
	}
	else
	{
	x_1=z[k_2-1];
	f_1=f_z[k_2-1];
	x_2=z[k_2];
	f_2=f_z[k_2];		
	f_s=liner_interpol(x_1, f_1, x_2, f_2, z_2);	
	
	d_z=(z_2-x_1);
	z_z=0.5*( f_s+f_1 )*d_z;
	S_0=S_0+z_z;		
	}


d_z=z[k_1+1]-z[k_1];

	for(i=k_1; i<(k_2-1); i++)
	{
	ch=0.5*( f_z[i+1]+f_z[i] )*d_z;
	S_0=S_0+ch;	
	}

return(S_0);	
}


void weno_x_p_5(bool type, int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma, double * dens_z)
{
	//_________weno_5 ��� ������������� ����������
	//_________type: true - ��������� �� � ���� (�� � �����������), false - ��������� �� � ���� (�� � �����������)
	//_________dens_z - ���������
	//________ i_mi..i_ma - �������� ������� ��  x, j_mi..j_ma - �������� ������� �� p
	//__________���������� ������������ ���������

	
int i, j, k_k, k_0, k_1, k_2, k_3, k_4, k_5;
	double S_0, a_0, d_z, f_1, f_2, f_3, f_4, f_5;	


switch ( type ) 
{
         case true:  		//����������� �� �
d_z=p[j_mi+1]-p[j_mi];
 
	for(i=i_mi; i<=i_ma; i++)
	{
		S_0=0.;
		for(j=j_mi; j<=(j_ma-1); j++)
		{
			k_0=j;
			
		k_1=k_0-2;
		k_k=(i-1)*n+k_1;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(i-1)*n+k_2;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(i-1)*n+k_3;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(i-1)*n+k_4;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(i-1)*n+k_5;
		f_5=dens[k_k];
		
		a_0=weno_5_integral(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;
		}	
	
		k_0=i;
	dens_z[k_0]=S_0;
	} 
       
            break; 
			 
         case false:  		//���������� �� �
d_z=x[i_mi+1]-x[i_mi];

	for(j=j_mi; j<=j_ma; j++)
	{
		S_0=0.;
		for(i=i_mi; i<=(i_ma-1); i++)
		{

			k_0=i;
			
		k_1=k_0-2;
		k_k=(k_1-1)*n+j;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(k_2-1)*n+j;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(k_3-1)*n+j;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(k_4-1)*n+j;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(k_5-1)*n+j;
		f_5=dens[k_k];
		
		a_0=weno_5_integral(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;
		
		}	
	
		k_0=j;
	dens_z[k_0]=S_0;
	}

            break; 	
	
	
}	
	return;
	
}

void weno_x_p(bool type, int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma, double * dens_z)
{
	//_________weno_3 ��� ������������� ����������
	//_________type: true - ��������� �� � ���� (�� � �����������), false - ��������� �� � ���� (�� � �����������)
	//_________dens_z - ���������
	//________ i_mi..i_ma - �������� ������� ��  x, j_mi..j_ma - �������� ������� �� p
	//__________���������� ������������ ���������

	
int i, j, k_k, k_0, k_1, k_2, k_3, k_4, k_5;
	double S_0, a_0, d_z, f_1, f_2, f_3, f_4, f_5;	


switch ( type ) 
{
         case true:  		//����������� �� �
d_z=p[j_mi+1]-p[j_mi];
 
	for(i=i_mi; i<=i_ma; i++)
	{

		S_0=0.;

//_________________������� �����
			k_0=j_mi;
			
		k_1=k_0-2;
		k_k=(i-1)*n+k_1;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(i-1)*n+k_2;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(i-1)*n+k_3;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(i-1)*n+k_4;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(i-1)*n+k_5;
		f_5=dens[k_k];
		
		a_0=weno_5_int_right(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;

			k_0=j_ma;
			
		k_1=k_0-2;
		k_k=(i-1)*n+k_1;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(i-1)*n+k_2;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(i-1)*n+k_3;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(i-1)*n+k_4;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(i-1)*n+k_5;
		f_5=dens[k_k];
		
		a_0=weno_5_int_left(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;
		
		for(j=(j_mi+1); j<=(j_ma-1); j++)
		{
			k_0=j;
			
		k_1=k_0-2;
		k_k=(i-1)*n+k_1;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(i-1)*n+k_2;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(i-1)*n+k_3;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(i-1)*n+k_4;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(i-1)*n+k_5;
		f_5=dens[k_k];
		
		a_0=weno_5_integral_conserv(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;
		}	
	
		k_0=i;
	dens_z[k_0]=S_0;
	} 
       
            break; 
			 
         case false:  		//���������� �� �
d_z=x[i_mi+1]-x[i_mi];

	for(j=j_mi; j<=j_ma; j++)
	{
		S_0=0.;
		
			k_0=i_mi;

		k_1=k_0-2;
		k_k=(k_1-1)*n+j;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(k_2-1)*n+j;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(k_3-1)*n+j;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(k_4-1)*n+j;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(k_5-1)*n+j;
		f_5=dens[k_k];
		
		a_0=weno_5_int_right(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;

			k_0=i_ma;
			
		k_1=k_0-2;
		k_k=(k_1-1)*n+j;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(k_2-1)*n+j;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(k_3-1)*n+j;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(k_4-1)*n+j;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(k_5-1)*n+j;
		f_5=dens[k_k];
		
		a_0=weno_5_int_left(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;
		
		for(i=(i_mi+1); i<=(i_ma-1); i++)
		{

			k_0=i;
			
		k_1=k_0-2;
		k_k=(k_1-1)*n+j;
		f_1=dens[k_k];
		
		k_2=k_0-1;
		k_k=(k_2-1)*n+j;
		f_2=dens[k_k];	
			
		k_3=k_0;
		k_k=(k_3-1)*n+j;
		f_3=dens[k_k];
				
		k_4=k_0+1;
		k_k=(k_4-1)*n+j;
		f_4=dens[k_k];
		
		k_5=k_0+2;
		k_k=(k_5-1)*n+j;
		f_5=dens[k_k];
		
		a_0=weno_5_integral_conserv(d_z, f_1, f_2, f_3, f_4, f_5);
		S_0=S_0+a_0;
		
		}	
	
		k_0=j;
	dens_z[k_0]=S_0;
	}

            break; 	
	
	
}	
	return;
	
}
//_____________________________________��������������
void density_x(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma, double * dens_x)
{
//_____________________ ������� ��������� ����������� �� � dens_x, ���������� �� �
//_____________________ dens - ��������� ��������� �����������
//_____________________ i_mi, i_ma, j_mi, j_ma - ������ ��������� �������, ��� ��������� �����
int i, j, k_k_1, k_k_2;
	double S_p, a_p;
	
	for(i=i_mi; i<=i_ma; i++)
	{
		S_p=0.;
		for(j=j_mi; j<=(j_ma-1); j++)
		{
		k_k_1=(i-1)*n+j;	
		k_k_2=(i-1)*n+j+1;
		a_p=0.5*( p[j+1] - p[j] )*( dens[k_k_1] + dens[k_k_2] );				//��������, ������ ������� ��� ������� �������
		S_p=S_p+a_p;		
		}	
	
	dens_x[i]=S_p;
	}
	
	return;
}

//______________________________________________________________________________________________
void density_p(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma, double * dens_p)
{
//_____________________ ������� ��������� ����������� �� p dens_p, ���������� �� x
//_____________________ dens - ��������� ��������� �����������
//_____________________ i_mi, i_ma, j_mi, j_ma - ������ ��������� �������, ��� ��������� �����
int i, j, k_k_1, k_k_2;
	double S_x, a_x;
	
	for(j=j_mi; j<=j_ma; j++)
	{
		S_x=0.;
		for(i=i_mi; i<=(i_ma-1); i++)
		{
		k_k_1=(i-1)*n+j;	
		k_k_2=(i+1-1)*n+j;
		a_x=0.5*( x[i+1] - x[i] )*( dens[k_k_1] + dens[k_k_2] );				//��������, ������ ������� ��� ������� �������
		S_x=S_x+a_x;		
		}	
	
	dens_p[j]=S_x;
	}
	
	return;
}



//______________________________________________________________________________________________
double full_probavility_w(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma)
{
//_____________________ ������� �������� �� ��������� ����������� 
//_____________________ dens - ��������� ��������� �����������
//_____________________ i_mi, i_ma, j_mi, j_ma - ������ �����, �� ������� �����������, ��� ��������� �����
int i, j, k_k_1, k_k_2;
	double S_p, a_p;
	
		double *dens_x = new double [m+1];
	//______________________________������� ��������� �� � ������
	for(i=i_mi; i<=i_ma; i++)
	{
		S_p=0.;
		for(j=j_mi; j<=(j_ma-1); j++)
		{
		k_k_1=(i-1)*n+j;	
		k_k_2=(i-1)*n+j+1;
		a_p=0.5*( p[j+1] - p[j] )*( dens[k_k_1] + dens[k_k_2] );				//��������, ������ ������� ��� ������� �������
		S_p=S_p+a_p;		
		}	
	
	dens_x[i]=S_p;
	}

	S_p=0.;
		for(i=i_mi; i<=(i_ma-1); i++)
		{
		a_p=0.5*( x[i+1] - x[i] )*( dens_x[i+1] + dens_x[i] );	
		S_p=S_p+a_p;
		}

	delete [] dens_x;	
	
	return(S_p);
}


//___________________________������ ����������� �� WENO
double full_probavility(int m, double * x, int n, double * p, double * dens, int i_mi, int i_ma, int j_mi, int j_ma)
{
//_____________________ ������� �������� �� ��������� ����������� 
//_____________________ dens - ��������� ��������� �����������
//_____________________ i_mi, i_ma, j_mi, j_ma - ������ �����, �� ������� �����������, ��� ��������� �����
int i, j, k_k_1, k_k_2;
	double S_p, a_p;
	
		double *dens_x = new double [m+1];
	//______________________________������� ��������� �� � ������

weno_x_p(true, m, x, n, p, dens, i_mi, i_ma, j_mi, j_ma, dens_x);
//density_x(m, x, n, p, dens, i_mi, i_ma, j_mi, j_ma, dens_x);
//___________________����������� �� �
//_______________extention
extrapol_1_dim_2(m, x, dens_x, i_mi, i_ma);
a_p=x[i_mi+1]-x[i_mi];
S_p=integr_calc(4, a_p, m, dens_x, i_mi, i_ma);

	delete [] dens_x;	
	
	return(S_p);
}

